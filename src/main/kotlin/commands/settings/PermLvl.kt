package commands.settings

import commands.Command
import commons.*
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class PermLvl : Command() {


    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val tc = event.textChannel
        val guild = event.guild

        if (args.size < 3) {
            sendErrorMessage(tc)
            return
        }

        val action = args[0].toLowerCase()
        if(action != "add" && action != "remove" ) {
            tc.sendMessage(error(guild, "Bitte als erstes Argument add oder remove angeben!"))
            return
        }

        val lvl = args[1].toIntOrNull()
        if(lvl != 1 && lvl != 2) {
            tc.sendMessage(error(guild, "Bitte eine 1 oder eine 2 als Argument für den Berechtigungslevel angeben!"))
            return
        }

        val inputroles = args.slice(2..args.lastIndex).map { it.replace(",", "") }
        val roles = ArrayList<Role>()

        val failed = ArrayList<String>()
        inputroles.forEach {
            val role = if(it.toLongOrNull() != null) {
                event.guild.getRoleById(it)
            } else {
                event.guild.getRolesByName(it, true).firstOrNull()
            }

            if(role == null) {
                failed.add(it)
            } else {
                roles.add(role)
            }
        }

        when(action) {
            "add" -> when(lvl) {
                1 -> setPERMROLES1(guild, roles)
                2 -> setPERMROLES2(guild, roles)
            }
            "remove" -> when(lvl) {
                1 -> removePERMROLES1(guild, roles)
                2 -> removePERMROLES2(guild, roles)
            }
        }

        if (roles.size > 0) {
            val names = roles.joinToString { it.name }
            tc.sendMessage(success(guild, "Die folgenden Rollen wurden erfolgreich für Berechtigungslevel $lvl gesetzt:\n" +
                   names)).queue()
        }

        if (failed.size > 0) {
            tc.sendMessage(error(guild, "Die folgenden Rollen konnten nicht identifiziert werden:\n" +
                    failed.joinToString() +
            "\nBitte eine gültige RollenID oder Rollennamen eingeben")).queue()
        }
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " add <Lvl> <Rolle1>, <Rolle2> ...** - `Setzt die Rollen für das Berechtigungslevel <Lvl> (1 = Moderatorenrechte, 2 = Adminrechte)`\n" +
                "$command remove <Lvl> <Rolle1>, <Rolle2> ...** - `Löscht die Rollen für das Berechtigungslevel <Lvl>`"
    }

    override fun description(): String {
        return "Setzt oder löscht Rollen für eine Berechtigungsstufe."
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.SETTINGS
    }

    override fun permission(): Int {
        return 2
    }
}