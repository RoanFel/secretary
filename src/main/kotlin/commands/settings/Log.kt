package commands.settings

import commands.Command
import commons.CMDTYPE
import data.GuildDB
import data.getGuild
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.jetbrains.exposed.sql.transactions.transaction

class Log : Command() {
    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if(args.isEmpty()) {
            sendErrorMessage(event.textChannel)
            return
        }

        val guild = getGuild(event.guild)
        if(guild.logchanId < 0) {
            event.channel.sendMessage(commons.error(event.guild, "Es ist kein Channel für Logs gesetzt, " +
                    "somit kann der Befehl nicht aufgeführt werden.\n" +
                    "Bitte erst mit !botchannels log einen Channel für den Log festlegen")).queue()
            transaction {
                guild.logEdit = false
                guild.logDelete = false
            }
            return
        }

        when(args[0]) {
            "delete" -> transaction { guild.logDelete = !guild.logDelete }
            "edit" -> transaction { guild.logEdit = !guild.logEdit }
            else -> {
                sendErrorMessage(event.textChannel)
            }
        }
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " delete** - `Schaltet das Logging von gelöschten Nachrichten an/aus`\n" +
                "$command edit** - `Schaltet das Logging von editierten Nachrichten an/aus`"
    }

    override fun description(): String {
        return "De-/Aktiviert die Logs von gelöschten oder editierten Nachrichten"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.SETTINGS
    }

    override fun permission(): Int {
        return 2
    }

}