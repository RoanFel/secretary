package commands.settings

import commands.Command
import commons.*
import net.dv8tion.jda.api.entities.Guild

import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class Prefix : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if (args.isEmpty()) {
            sendErrorMessage(event.textChannel)
            return
        }
        setPREFIX(args[0], event.guild)
        event.textChannel.sendMessage(success(event.guild,"Prefix wurde auf ${args[0]} geändert")).queue()
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " <Prefix>** - `Setzt den Prefix für den Server auf <Prefix>`"
    }

    override fun description(): String {
        return "Setzt den Prefix für den Server"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.SETTINGS
    }

    override fun permission(): Int {
        return 2
    }
}