package commands.settings

import commands.Command
import commons.CMDTYPE
import commons.setSERVERJOINMESSAGE
import commons.setSERVERLEAVEMESSAGE
import commons.success
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.message.MessageReceivedEvent


class Msg : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if (args.size < 2) {
            sendErrorMessage(event.textChannel)
            return
        }

        val action = args[0]
        val message = args.slice(1..args.lastIndex).joinToString { " " }

        when(action) {
            "join" -> {
                setSERVERJOINMESSAGE(message, event.guild)
                event.textChannel.sendMessage(success(event.guild, "Willkommensnachricht wie folgt gesetzt:\n" +
                        message)).queue()
            }
            "leave" -> {
                setSERVERLEAVEMESSAGE(message, event.guild)
                event.textChannel.sendMessage(success(event.guild, "Abschiedsnachricht wie folgt gesetzt:\n" +
                            message)).queue()
            }
            else -> event.textChannel.sendMessage(commons.error(event.guild,
                "Als erstes Argument join oder leave angeben!")).queue()
        }

    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " join <Text>** - `Setzt <Text> als Willkommensnachricht für den Server`\n" +
                "$command leave <Text>** - `Setzt <Text> als Abschiedsnachricht für den Server`\n" +
                "```Schreibe [USER] um den User der gejoint ist zu erwähnen und [SERVER] um den Servernamen in die Nachricht zu schreiben.```"
    }

    override fun description(): String {
        return "Setzt die Willkommensnachricht für den Server"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.SETTINGS
    }

    override fun permission(): Int {
        return 2
    }
}