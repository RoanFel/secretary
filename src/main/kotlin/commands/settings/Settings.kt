package commands.settings

import commands.Command
import commons.CMDTYPE
import commons.listSettings
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.message.MessageReceivedEvent


class Settings : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        listSettings(event)
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Zeigt die aktuellen Einstellungen des Servers an`"
    }

    override fun description(): String {
        return "Zeigt alle aktuellen Einstellungen des Servers an"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.SETTINGS
    }

    override fun permission(): Int {
        return 1
    }
}