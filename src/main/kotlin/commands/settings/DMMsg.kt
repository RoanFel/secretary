package commands.settings

import commands.Command
import commons.CMDTYPE
import commons.setPRIVATEJOINMESSAGE
import commons.setPRIVATELEAVEMESSAGE
import commons.success
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class DMMsg : Command() {
    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if (args.size < 2) {
            sendErrorMessage(event.textChannel)
            return
        }

        val action = args[0]
        val message = args.slice(1..args.lastIndex).joinToString { " " }

        when(action) {
            "join" -> {
                setPRIVATEJOINMESSAGE(message, event.guild)
                event.textChannel.sendMessage(success(event.guild, "Willkommensnachricht wie folgt gesetzt:\n" +
                        message)).queue()
            }
            "leave" -> {
                setPRIVATELEAVEMESSAGE(message, event.guild)
                event.textChannel.sendMessage(success(event.guild, "Abschiedsnachricht wie folgt gesetzt:\n" +
                        message)).queue()
            }
            else -> event.textChannel.sendMessage(commons.error(event.guild,
                "Als erstes Argument join oder leave angeben!")).queue()
        }
    }

    override fun description(): String {
        return "Setzt die Willkommens-/Abschiedsnachricht die direkt an den User geschickt werden."
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " join <Text>** - `Setzt eine Willkommensnachricht, die an den neuen User direkt geschickt wird`\n" +
                "$command leave <Text>** - `Setzt eine Abschiedsnachricht, die an den User der geht direkt geschickt wird`"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.SETTINGS
    }

    override fun permission(): Int {
        return 2
    }
}