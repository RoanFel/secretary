package commands.settings

import commands.Command
import commons.*
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class BotChannels : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if(args.isEmpty() || args.size < 2) {
            sendErrorMessage(event.textChannel)
            return
        }
        val chanId = args[1]
        val chan = if(chanId.toLongOrNull() != null) {
            if(args[0].toLowerCase() == "privatvc") {
                event.guild.getVoiceChannelById(chanId)
            } else {
                event.guild.getTextChannelById(chanId)
            }
        } else {
            event.guild.getTextChannelsByName(chanId, true).firstOrNull()
        }

        if(chan == null) {
            event.textChannel.sendMessage(error(event.guild, "Es gibt keinen Channel mit der ID " +
                    "oder dem Namen $chanId")).queue()
            return
        }

        when(args[0].toLowerCase()) {
            "cmdlog" -> setCmdlogchan(chan.idLong, event.guild)
            "voicelog" -> setVoicelogchan(chan.idLong, event.guild)
            "log" -> setlogchan(chan.idLong, event.guild)
            "support" -> setSupportchan(chan.idLong, event.guild)
            "feedback" -> setFeedbackchan(chan.idLong, event.guild)
            "warnlog" -> setWarnlogchan(chan.idLong, event.guild)
            "joinmsg" -> setJoinmsgchan(chan.idLong, event.guild)
            "leavemsg" -> setLeavemsgchan(chan.idLong, event.guild)
            "privatvc" -> setPrivatvcchan(chan.idLong, event.guild)
            else -> sendErrorMessage(event.textChannel)
        }
        event.textChannel.sendMessage(success(event.guild, "Der Channel ${chan.name} mit der ID " +
                "${chan.id} wurde erfolgreich als Channel für ${args[0]} gesetzt")).queue()
    }



    override fun help(guild: Guild): String {
        return super.help(guild) + " cmdlog <Chan ID>** - `Setzt den Channel in dem die übermittelten Befehle des Bots geloggt werden`\n" +
                "$command voicelog <ChanID>** - `Setzt den Channel für den VoiceLog (Betreten, Verlassen, Switchen von Voicechanneln`\n" +
                "$command log <ChanID>** - `Setzt den Channel für den log für gelöschte/editierte Nachrichten`\n" +
                "$command support <ChanID>** - `Setzt den Channel für das Ticketsystem`\n" +
                "$command feedback <ChanID>** - `Setzt den Channel für das Feedback (automatisches setzen von Reaktionen)`\n" +
                "$command warnlog <ChanID>** - `Setzt den Channel zum loggen der Warns`\n" +
                "$command joinmsg <ChanID>** - `Setzt den Channel für die Willkommensnachrichten`\n" +
                "$command leavemsg <ChanID>** - `Setzt den Channel für die Abschiedsnachrichten`\n" +
                "$command privatvc <ChanID>** - `Setzt den Channel für die privaten Voicechannel`"
    }

    override fun description(): String {
        return "Setzt neue Channel für die vom Bot benötigten channel"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.SETTINGS
    }

    override fun permission(): Int {
        return 2
    }

}