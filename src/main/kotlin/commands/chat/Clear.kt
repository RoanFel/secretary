package commands.chat

import commands.Command
import commons.CMDTYPE
import commons.deleteMessage
import commons.error
import commons.success
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageHistory
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

/**
 * Command to Clear Messages in a Channel
 */
class Clear : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val history = MessageHistory(event.textChannel)
        var msgs: List<Message>
        var message: Message? = null
        val int: Int? = if (args.isNotEmpty()) args[0].toIntOrNull() else 0
        if (args.size == 1 && args[0].equals("all", ignoreCase = true)) {
            while (true) {
                msgs = history.retrievePast(100).complete()
                if (msgs.isEmpty()) break
                msgs.forEach { it.delete().queue() }
            }

            message = event.textChannel.sendMessage(success(event.guild, "Erfolgreich alle Nachrichten gelöscht!")).complete()
        } else if (int != null && int < 2) {
            event.message.delete().queue()
            history.retrievePast(2).complete().forEach { it.delete().queue() }
            message = event.textChannel.sendMessage(success(event.guild, "Erfolgreich die letzte Nachricht gelöscht!")).complete()
        } else if (args.size == 2) { // 24/03/2013 21:54
            val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")

            val date: LocalDateTime = try {
                LocalDateTime.parse(args.joinToString(" "), formatter)
            } catch (e: DateTimeParseException) {
                event.textChannel.sendMessage(error(event.guild, "Falsches Datumsformat\n\n" +
                        "Bitte gebe das Datum im richtigen Format ein:\n" + formatter.format(LocalDateTime.now()))
                ).queue()
                return
            }
            while (true) {
                msgs = history.retrievePast(100).complete()
                if (msgs.isEmpty()) break
                msgs.forEach {
                    if (it.timeCreated.toLocalDateTime().isBefore(date)) it.delete().queue()
                }
            }

            message = event.textChannel.sendMessage(success(event.guild,
                "Erfolgreich alle Nachrichten vor dem ${date.format(formatter)} gelöscht!")).complete()
        } else if (int != null && int <= 100) {
            event.message.delete().queue()
            event.textChannel.deleteMessages(history.retrievePast(int).complete()).queue()
            message = event.textChannel.sendMessage(success(event.guild,
                "Erfolgreich " + args[0] + " Nachrichten gelöscht!")).complete()
        } else {
            event.textChannel.sendMessage(error(event.guild, "Argumente konnten nicht verarbeitet werden" +
                    "Bitte nur die Argumente all, {Zahl zwischen 1-100} oder Datum nach dem Format dd.MM.yyyy HH:mm eingeben")
            ).queue()
        }
        if(message != null) deleteMessage(message, 10000)
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Löscht die letzte Nachricht`\n" +
                "$command all** - `Löscht alle Nachrichten im Channel`\n" +
                "$command <Zahl (1-100)>** - `Löscht die letzten <Zahl> Nachrichten`\n" +
                "$command <Datum (Format 01.03.2020 14:00)>** - `Löscht alle Nachrichten vor <Datum>`\n"
    }

    override fun description(): String {
        return "Löscht Nachrichten"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.CHATUTILS
    }

    override fun permission(): Int {
        return 1
    }
}