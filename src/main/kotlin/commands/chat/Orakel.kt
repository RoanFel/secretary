package commands.chat

import commands.Command
import commons.CMDTYPE
import commons.createEmbed
import commons.getServerFooter
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color

class Orakel : Command() {

    val list = listOf<String>("Möglicherweise ja", "Dies ist schwer zu sagen", "Definitiv und hundertprozentig ja",
        "Dies halte ich für keine gute Idee", "Diese Frage kann ich nicht beantworten")

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if(args.isEmpty()) {
            sendErrorMessage(event.textChannel)
            return
        }

        event.textChannel.sendMessage(createEmbed("Das Dunkel lichtet sich", Color.MAGENTA, list.random(),
            "Das Orakel","https://static.vecteezy.com/system/resources/previews/000/187/123/original/fortune-teller-illustration-vector.jpg",
        getServerFooter(event.guild))).queue()
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " <Frage>** - Stelle eine Frage auf die du eine Antwort erhalten willst"
    }

    override fun description(): String {
        return "Frage das Orakel jede Frage und erhalte deine Antwort"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.CHATUTILS
    }

}