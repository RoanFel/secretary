package commands.etc

import commands.Command
import commons.CMDTYPE
import commons.createEmbed
import core.Secretary
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed.Field
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color
import java.io.*

class BotStats : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val commandsSize = "" + Secretary.commands.size
        val serversRunning = "" + event.jda.guilds.size
        var membersDeserving = 0
        event.jda.guilds.forEach { g: Guild -> membersDeserving += g.members.count() }

        val fields = listOf(
            Field("Registrierte Befehle",   commandsSize, false),
            Field("Läuft auf Servern", serversRunning, false),
            Field("Dient Mitgliedern", "" + membersDeserving, false),
            Field("Nachrichten produziert", "" + messagesProcessed, false),
            Field("Befehle aufgeführt", "" + commandsExecuted, false)
        )

        event.textChannel.sendMessage(
            createEmbed("**RoanBot Statistiken**", Color.CYAN, "", event.guild.selfMember.effectiveName,
                event.guild.selfMember.user.effectiveAvatarUrl, fields = fields)).queue()
        membersDeserving = 0
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Zeigt die Statistiken des Bots an`"
    }

    override fun description(): String {
        return "Zeigt die Statisiken des Bots an."
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.ETC
    }

    companion object {
        private val f = File("botstatics.donotdelete")
        private var messagesProcessed: Long = 0
        private var commandsExecuted: Long = 0
        fun incrementMessagesProcessed() {
            messagesProcessed++
            save()
        }

        fun incrementCommandsExecuted() {
            commandsExecuted++
            save()
        }

        private fun save() {
            if (!f.exists()) try {
                f.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            try {
                val bw = BufferedWriter(FileWriter(f))
                bw.write(messagesProcessed.toString() + "\n" + commandsExecuted)
                bw.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        private fun stats(): LongArray {
            if (!f.exists()) return longArrayOf(0, 0)
            try {
                val br = BufferedReader(FileReader(f))
                return longArrayOf(br.readLine().toLong(), br.readLine().toLong())
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return longArrayOf(0, 0)
        }

        fun load() {
            messagesProcessed = stats()[0]
            commandsExecuted = stats()[1]
        }
    }
}