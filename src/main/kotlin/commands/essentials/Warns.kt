package commands.essentials

import commands.Command
import commons.*
import core.PermCheck
import data.UserWarn
import data.getUserWarns
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed.Field
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.awt.Color

class Warns : Command() {

    private fun strike (old: Boolean): String{
        return if(old) "~~" else ""
    }

    private fun sendWarnsMessage(event: MessageReceivedEvent, user: User, warns: List<UserWarn>) {
        val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")
        val fields = ArrayList<Field>()
        var i = 1
        warns.forEach {
            val old = DateTime.now().isAfter(it.enddate)
            fields.add(Field("${strike(old)}$i.${strike(old)}",
                "${strike(old)}WarnID: ${it.warnId}${strike(old)}\n" +
                "${strike(old)}Grund: ${it.reason}${strike(old)}\n" +
                        "${strike(old)}Punkte: ${it.points}${strike(old)}\n" +
                        "${strike(old)}Läuft bis: ${formatter.print(it.enddate)}${strike(old)}", false))
            i++
        }
        val embed = createEmbed("Verwarnungen", Color.CYAN,
            "Verwarnungen für ${user.asMention}", getAuthor(event.guild), event.guild.iconUrl,
            getServerFooter(event.guild), fields)
        if(user == event.author) {
            user.openPrivateChannel().complete().sendMessage(embed).queue()
        } else {
            event.textChannel.sendMessage(embed).queue()
        }

    }

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val user = if(args.isEmpty()) {
            event.author
        } else {
            val userId = args[0]
            if(userId.toLongOrNull() != null) {
                event.jda.getUserById(userId)
            } else {
                event.message.mentionedUsers.firstOrNull()
            }
        }

        if(user == null) {
            event.textChannel.sendMessage(error(event.guild, "Es gibt keinen User mit der ID " +
                    "oder dem Namen ${args[0]}")).queue()
            return
        }

        val warns = getUserWarns(event.guild.idLong, user.idLong)

        sendWarnsMessage(event, user, warns)
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Ruft die Eigenen Verwarnungen ab`\n" +
                "${PermCheck.getPermPre(1) + "**" + getPREFIX(guild) + toString()} <UserID/@User>** - `Ruft die Verwarnungen von <User> ab (nur für Mods verwendbar)`"
    }

    override fun description(): String {
        return "Ruft die Verwarnungen von einem selbst oder (nur für Mods) von einem anderen User ab."
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.ESSENTIALS
    }

}