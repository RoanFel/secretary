package commands.essentials

import club.minnced.discord.webhook.WebhookClientBuilder
import club.minnced.discord.webhook.send.WebhookEmbed
import club.minnced.discord.webhook.send.WebhookEmbedBuilder
import commands.Command
import commons.*
import core.PermCheck
import core.Secretary
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Icon
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.io.File
import java.util.*
import kotlin.concurrent.schedule


class Help : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if (args.isNotEmpty()) {
            val cmd = Secretary.commands[args[0]]
            if (cmd != null) {
                event.textChannel.sendMessage(createEmbed("Hilfe zu $cmd", getCMDTYPEColors(cmd.commandType()), cmd.help(event.guild))).queue()
            } else {
                event.textChannel.sendMessage(error(event.guild,
                    ":warning:  The command list does not contains information for the command *" +
                            getPREFIX(event.guild) + args[0] + "* !")).queue()
            }
            return
        }

        val ciams = StringBuilder()

        try {
            val botname = event.guild.selfMember.effectiveName
            val webhook = event.textChannel.createWebhook(botname).setAvatar(Icon.from(File("headset.png"))).complete()
            val builder = WebhookClientBuilder(webhook.url)

            builder.setThreadFactory { job: Runnable? ->
                val thread = Thread(job)
                thread.name = "Hello"
                thread.isDaemon = true
                thread
            }
            builder.setWait(true)
            val client = builder.build()
            val embeds = mutableListOf<WebhookEmbed>()
            val perms1 = getPERMROLES1Names(event.guild)
            val perms2 = getPERMROLES2Names(event.guild)
            embeds.add(WebhookEmbedBuilder().setColor(0xFFFFFF).setDescription(":clipboard:  __**Liste der Befehle**__  :clipboard: \n\n" +
                    "***Legende:***\n" +
                    "  :white_small_square:  -  Nutzbar für jeden\n" +
                    "  :small_blue_diamond:  -  Nur für Rollen: \n`" + perms1.joinToString("\n-   ", prefix = "-   ") + "`\n" +
                    "  :small_orange_diamond:  -  Nur für Rollen: \n`" + perms2.joinToString("\n-   ", prefix = "-   ") + "`\n" +
                    "  :small_red_triangle_down:  -  Nur für den Owner des Servers\n"
                    + "\n\n___").build())

            val types = CMDTYPE.values()

            for (i in 0..CMDTYPE.values().lastIndex) {
                val cmdType = types[i]
                ciams.delete(0, ciams.length)
                ciams.append("**${cmdType.cmd}**\n")
                val list = Secretary.commands.filter { it.value.commandType() == cmdType && !PermCheck.check(it.value.permission(), event) }
                if (list.isEmpty()) continue
                list.forEach {
                    ciams.append(
                            "${PermCheck.getPermPre(it.value.permission())}**${it.key}**   -   `${it.value.description()}`\n"
                    )
                }
                embeds.add(WebhookEmbedBuilder().setColor(getCMDTYPEColors(cmdType).rgb).setDescription(ciams.toString()).build())
            }

            client.send(embeds)

            Timer().schedule(10000) { webhook.delete().queue() }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Gibt eine Übersicht aller Befehle aus`\n" +
                "$command <Befehl>** - `Gibt den Hilfstext für <Befehl> aus`"
    }

    override fun description(): String {
        return "Hilfe über die Befehle des Bots"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.ESSENTIALS
    }

}