package commands.essentials

import commands.Command
import commons.*
import core.PermCheck
import data.getGuild
import net.dv8tion.jda.api.OnlineStatus
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.*
import net.dv8tion.jda.api.entities.MessageEmbed.Field
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.jetbrains.exposed.sql.transactions.transaction
import java.awt.Color
import java.util.stream.Collectors


class Stats : Command() {

    private class GuildStats(g: Guild) {
        private val l = g.members

        val name = g.name
        val id = g.id
        val region = g.region.name
        val avatar = g.iconUrl
        val textChans = g.textChannels.size
        val voiceChans = g.voiceChannels.size
        val categories = g.categories.size
        val rolesCount = g.roles.size
        val afk = g.afkChannel?.name ?: "not set"
        val owner = g.owner
        val roles: String = g.roles.stream().filter { r -> !r.name.contains("everyone") }
                .map { r -> String.format("%s (`%d`)", r.name, getMembsInRole(r)) }
                .collect(Collectors.joining(", "))
        val all = l.size
        val users = l.stream().filter { m -> !m.user.isBot }.count()
        val onlineUsers = l.stream().filter { m -> !m.user.isBot && m.onlineStatus != OnlineStatus.OFFLINE }.count()
        val bots = l.stream().filter { m -> m.user.isBot }.count()
        val onlineBots = l.stream().filter { m -> m.user.isBot && m.onlineStatus != OnlineStatus.OFFLINE }.count()

        fun getMembsInRole(r: Role): Long {
            return r.guild.members.stream().filter { m -> m.roles.contains(r) }.count()
        }

    }

    private fun getEveryonePermissions(): ArrayList<Permission> {
        return arrayListOf(Permission.MANAGE_CHANNEL, Permission.MANAGE_PERMISSIONS, Permission.VOICE_CONNECT)
    }

    private fun getBotPermissions(): ArrayList<Permission> {
        return arrayListOf(Permission.MANAGE_CHANNEL, Permission.VIEW_CHANNEL, Permission.VOICE_CONNECT)
    }

    private fun createStatsVoice(event: MessageReceivedEvent, name: String, count: Int, cat: Category, pos: Int): VoiceChannel {
        val mvc = event.guild.voiceChannels.find { it.name.startsWith(name) }
        val selfRole: IPermissionHolder = event.guild.selfMember.roles.find { it.name == event.jda.selfUser.name } ?: event.guild.selfMember
        if(mvc == null) {
            return event.guild.createVoiceChannel(name + count).setParent(cat).setPosition(pos)
                    .addPermissionOverride(event.guild.publicRole, arrayListOf(Permission.VIEW_CHANNEL), getEveryonePermissions())
                    .addPermissionOverride(selfRole, null, getBotPermissions() ).complete()
        } else {
            mvc.manager.setName(name + count).setParent(cat).queue()
        }
        return mvc
    }

    private fun saveStatsCategory(guild: Guild, cat: Category, chans: List<VoiceChannel>) {
        transaction {
            val g = getGuild(guild)
            g.statscategoryId = cat.idLong
            g.statsmembersId = chans[0].idLong
            g.statsmembersonlyId = chans[1].idLong
            g.statsbotsId = chans[2].idLong
            g.statsonlineId = chans[3].idLong
            g.statsvoiceId = chans[4].idLong
        }
    }

    private fun getStatsCategory(guild: Guild): Category? {
        return transaction {
            val g = getGuild(guild)
            guild.getCategoryById(g.statscategoryId)
        }
    }

    private fun deleteSetup(cat: Category, event: MessageReceivedEvent) {
        cat.channels.forEach { it.delete().queue() }
        cat.delete().queue()
        transaction {
            val guild = getGuild(event.guild)
            guild.statscategoryId = -1
            guild.statsmembersId = -1
            guild.statsmembersonlyId = -1
            guild.statsbotsId = -1
            guild.statsonlineId = -1
            guild.statsvoiceId = -1
        }
        event.channel.sendMessage(success(event.guild, "Setup für Statistik erfolgreich gelöscht")).queue()
    }

    private fun setupStats(event: MessageReceivedEvent) {
        var cat = getStatsCategory(event.guild)
        if(cat != null) {
            deleteSetup(cat, event)
            return
        }
        cat = event.guild.createCategory("Statistiken \uD83D\uDCCA").complete()
        val cats =  event.guild.categories
        event.guild.modifyCategoryPositions().selectPosition(cat.position).moveUp(cats.size-1).queue()

        val members = event.guild.members
        val membersCounts = members.count()
        val bots = members.filter { it.user.isBot }.count()
        val nonBots = membersCounts - bots
        val online = event.guild.members.filter { it.onlineStatus != OnlineStatus.OFFLINE }.count()
        var memberinvoice = 0
        event.guild.voiceChannels.forEach { memberinvoice += it.members.size }
        val chans: MutableList<VoiceChannel> = ArrayList()
        var i = 1
        val map = mapOf(Pair(STATICS.STATSMEMBERS, membersCounts), Pair(STATICS.STATSNONBOTS, nonBots), Pair(STATICS.STATSBOTS, bots),
                Pair(STATICS.STATSONLINE, online), Pair(STATICS.STATSINVOICE, memberinvoice))
        map.forEach {chans.add(createStatsVoice(event, it.key, it.value, cat, i++))}

        saveStatsCategory(event.guild, cat, chans)
        event.channel.sendMessage(success(event.guild, "Setup für Statisik erfolgreich gesetzt")).queue()
    }

    private fun toggleChannel(event: MessageReceivedEvent, name: String, count: Int, cat: Category, id: Long, pos: Int): Long {
        val chan = event.guild.getVoiceChannelById(id)

        return if(chan == null) {
            val vc = createStatsVoice(event, name, count, cat, pos)
            event.channel.sendMessage(success(event.guild, "Erfolgreich den Statistikchannel $name aktiviert")).queue()
            vc.idLong
        } else {
            chan.delete().queue()
            event.channel.sendMessage(success(event.guild, "Erfolgreich den Statistikchannel $name deaktiviert")).queue()
            -1
        }
    }

    private fun toggleStatsChannel(event: MessageReceivedEvent, arg: String) {
        val cat = getStatsCategory(event.guild)
        if(cat == null) {
            event.channel.sendMessage(error(event.guild, "Es ist kein Setup vorhanden!\n" +
                    "Bitte zuerst mit ${getPREFIX(event.guild)}stats setup ein setup erstellen.")).queue()
            return
        }
        var name: String
        var count = 0
        var id: Long
        transaction {
            val guild = getGuild(event.guild)
            when(arg){
                "allmembers" -> {
                    name = STATICS.STATSMEMBERS
                    count = event.guild.members.count()
                    id = guild.statsmembersId
                    guild.statsmembersId = toggleChannel(event, name, count, cat, id, 1)
                }
                "members" -> {
                    name = STATICS.STATSNONBOTS
                    count = event.guild.members.filter { !it.user.isBot }.count()
                    id = guild.statsmembersonlyId
                    guild.statsmembersonlyId = toggleChannel(event, name, count, cat, id,2)
                }
                "bots" -> {
                    name = STATICS.STATSBOTS
                    count = event.guild.members.filter { it.user.isBot }.count()
                    id = guild.statsbotsId
                    guild.statsbotsId = toggleChannel(event, name, count, cat, id, 3)
                }
                "online" -> {
                    name = STATICS.STATSONLINE
                    count = event.guild.members.filter { it.onlineStatus != OnlineStatus.OFFLINE }.count()
                    id = guild.statsonlineId
                    guild.statsonlineId = toggleChannel(event, name, count, cat, id,4)
                }
                "voice" -> {
                    name = STATICS.STATSINVOICE
                    event.guild.voiceChannels.forEach { count += it.members.size }
                    id = guild.statsvoiceId
                    guild.statsvoiceId = toggleChannel(event, name, count, cat, id, 5)
                    }
                else -> {
                    event.channel.sendMessage(error(event.guild, help(event.guild))).queue()
                    return@transaction null
                }
            }
        }

    }

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if(args.size == 1) {
            when(args[0]) {
                "setup" ->
                    if(PermCheck.check(2, event)) {
                        PermCheck.sendPermMessage(2, event)
                        return
                    } else {
                        setupStats(event)
                    }
                else -> toggleStatsChannel(event, args[0])
            }
            return
        }

        val g = event.guild
        val gs = GuildStats(g)
        val usersText = java.lang.String.format(
                "**Alle User:**   %d\n" +
                        "**Mitglieder:**   %d   (Online:  %d)\n" +
                        "**Bots:**   %d   (Online:  %d)",
                gs.all, gs.users, gs.onlineUsers, gs.bots, gs.onlineBots
        )

        val fields = listOf(
            Field("Name:", gs.name, false),
            Field("ID:", gs.id, false),
            Field("Owner:", gs.owner?.user?.name + "#" + gs.owner?.user?.discriminator, false),
            Field("Server Region:", gs.region, false),
            Field("Channels", "**Text Channels:**  " + gs.textChans + "\n**Voice Channels:**  " +
                    gs.voiceChans + "\n**Kategorien:**  " + gs.categories, false),
            Field("User:", usersText, false),
            Field("Rollen (" + gs.rolesCount + "): ", gs.roles, false),
            Field("AFK Channel", gs.afk, false),
            Field("Server Avatar", gs.avatar, false)
        )
        val eb = createEmbed(gs.name + "  -  Server Statistiken", Color.CYAN, "",
            getAuthor(g), gs.avatar, fields = fields)
        event.textChannel.sendMessage(eb).queue()
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Zeigt die Statistik des Servers an`\n" +
                "${PermCheck.getPermPre(2) + "**" + getPREFIX(guild) + toString()} setup** - `Aktiviert/Deaktiviert die Statistikchannels des Servers`\n" +
                "${PermCheck.getPermPre(2) + "**" + getPREFIX(guild) + toString()} allmembers** - `Statistikchannel über alle Mitglieder ein-/ausschalten`\n" +
                "${PermCheck.getPermPre(2) + "**" + getPREFIX(guild) + toString()} members** - `Statistikchannel über Mitglieder ohne Bots ein-/ausschalten`\n" +
                "${PermCheck.getPermPre(2) + "**" + getPREFIX(guild) + toString()} bots** - `Statistikchannel über Bots ein-/ausschalten`\n" +
                "${PermCheck.getPermPre(2) + "**" + getPREFIX(guild) + toString()} online** - `Statistikchannel über Mitglieder die online sind ein-/ausschalten`\n" +
                "${PermCheck.getPermPre(2) + "**" + getPREFIX(guild) + toString()} voice** - `Statistikchannel über Mitglieder im Voice ein-/ausschalten`\n"
    }

    override fun description(): String {
        return "Statistiken des Servers"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.ESSENTIALS
    }
}