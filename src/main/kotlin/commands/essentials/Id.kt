package commands.essentials

import commands.Command
import commons.*
import net.dv8tion.jda.api.entities.*
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color
import java.util.stream.Collectors


class Id : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val tc: TextChannel = event.textChannel
        val guild: Guild = event.guild

        if (args.isEmpty()) {
            tc.sendMessage(error(event.guild, help(event.guild))).queue()
            return
        }

        val query = java.lang.String.join(" ", *args).toLowerCase()

        val roles = guild.roles.stream().filter { r -> r.name.toLowerCase().contains(query) }.collect(Collectors.toList())
        val tchans = guild.textChannels.stream().filter { c -> c.name.toLowerCase().contains(query) }.collect(Collectors.toList())
        val vchans = guild.voiceChannels.stream().filter { v -> v.name.toLowerCase().contains(query) }.collect(Collectors.toList())
        val membs = guild.members.stream().filter { m ->
            (m.nickname
                    ?: m.effectiveName).toLowerCase().contains(query) || m.effectiveName.toLowerCase().contains(query)
        }.collect(Collectors.toList())

        val list: ArrayList<MessageEmbed.Field> = arrayListOf(
            MessageEmbed.Field("Roles", if (roles.size == 0) "*No matches found*" else roles.stream().map { e: Role -> String.format("%s  -  `%s`", e.name, e.id) }.collect(Collectors.joining("\n")), false),
            MessageEmbed.Field("Voice Channels", if (vchans.size == 0) "*No matches found*" else vchans.stream().map { e: VoiceChannel -> String.format("%s  -  `%s`", e.name, e.id) }.collect(Collectors.joining("\n")), false),
            MessageEmbed.Field("Text Channels", if (tchans.size == 0) "*No matches found*" else tchans.stream().map { e: TextChannel -> String.format("%s  -  `%s`", e.name, e.id) }.collect(Collectors.joining("\n")), false),
            MessageEmbed.Field("Members", if (membs.size == 0) "*No matches found*" else membs.stream().map { e: Member -> String.format("%s  -  `%s`", e.effectiveName, e.user.id) }.collect(Collectors.joining("\n")), false)
        )

        tc.sendMessage(createEmbed("IDs", Color.CYAN, String.format("Found results for search query ```%s```", query),
            getAuthor(event.guild), event.guild.iconUrl, getServerFooter(event.guild), list)).queue()
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " <Name>** - `Gibt die ID von allen Elementen des Servers aus die <Name> im Namen enthalten`"
    }

    override fun description(): String {
        return "Gibt die IDs von Elementen des Servers anhand ihres Namens wieder"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.ESSENTIALS
    }
}