package commands.essentials

import commands.Command
import commons.*
import core.PermCheck
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed.Field
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color
import java.time.format.DateTimeFormatter


class UserInfo : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val memb = (if (args.isNotEmpty()) {
            if(args[0].toLongOrNull() != null) {
                event.guild.getMemberById(args[0])
            } else {
                event.message.mentionedMembers.firstOrNull()
            }
        } else {
            event.member
        })

        if(memb == null) {
            event.textChannel.sendMessage(error(event.guild, "Es gibt keinen User mit der ID " +
                    "oder dem Namen ${args[0]}")).queue()
            return
        }

        if (event.author.idLong != memb.idLong && PermCheck.check(1, event)) {
            PermCheck.sendPermMessage(1, event)
            return
        }

        val name = memb.effectiveName
        val tag = memb.user.name + "#" + memb.user.discriminator
        val guildJoinDate = memb.timeJoined.format(DateTimeFormatter.RFC_1123_DATE_TIME)
        val discordJoinDate = memb.user.timeCreated.format(DateTimeFormatter.RFC_1123_DATE_TIME)
        val id = memb.user.id
        val status = memb.onlineStatus.key
        var roles = ""
        val activity: String
        val avatar = memb.user.effectiveAvatarUrl
        activity = try {
            memb.activities.joinToString(", ") { a -> a.name }
        } catch (e: Exception) {
            "-/-"
        }
        for (r in memb.roles) {
            roles += r.name + ", "
        }
        roles = if (roles.isNotEmpty()) roles.substring(0, roles.length - 2) else "No roles on this server."

        val fields = arrayListOf(
            Field("Name / Nickname", name, false),
            Field("User Tag", tag, false),
            Field("ID", id, false),
            Field("Aktueller Status", status, false),
            Field("Aktuelle Aktivität", activity, false),
            Field("Rollen", roles, false),
            Field("Server Berechtigungslevel", PermCheck.getLvl(memb).toString() + "", false),
            Field("Server beigetreten", guildJoinDate, false),
            Field("Discord beigetreten", discordJoinDate, false),
            Field("Avatar-URL", avatar, false)
        )

        val em = createEmbed("User Informationen", Color.CYAN,":spy:   **für " + memb.user.name + ":**",
            getAuthor(event.guild),event.guild.iconUrl, getServerFooter(event.guild), fields)

        event.textChannel.sendMessage(EmbedBuilder(em).setThumbnail(avatar).build()).queue()
    }

    override fun help(guild: Guild): String {
        return super.help(guild) +  "** - `Gibt Infos über einen selbst wieder`\n" +
                "${PermCheck.getPermPre(1) + "**" + getPREFIX(guild) + toString()} <UserID/@User>** - `Gibt Infos über <User> wieder (nur für Mods verwendbar)`"
    }

    override fun description(): String {
        return "Erhalte Informationen über einen User"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.ESSENTIALS
    }
}