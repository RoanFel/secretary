package commands.essentials

import commands.Command
import commons.CMDTYPE
import commons.STATICS
import commons.createEmbed
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed.Field
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color


class Info : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val fields = arrayListOf(
            Field("Aktuelle Version", STATICS.VERSION, true),
            Field("Copyright","Coded by Roan-Fel.\n© 2020 Dennis Schubert.", false),
            Field("Libraries and Dependencies",
                " -  JDA  *(https://github.com/DV8FromTheWorld/JDA)*\n", false),
            Field("Bug Reporting / Idea Suggestion",
                "Wenn Bugs auftreten bitte hier melden:\n" +
                        " -  E-Mail:  hansgunter@mailbox.org\n" +
                        " -  Discord:  Roan-Fel#2859", false)
        )
        event.textChannel.sendMessage(
            createEmbed("Bot-Info", Color.CYAN, ":robot:   __**Die Sekretärin** JDA Discord Bot__",
            event.guild.selfMember.effectiveName, event.guild.selfMember.user.effectiveAvatarUrl, fields = fields)

        ).queue()


    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Gibt Infos über den Bot aus`"
    }

    override fun description(): String {
        return "Info über den Bot"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.ESSENTIALS
    }
}