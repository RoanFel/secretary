package commands.essentials

import commands.Command
import commons.CMDTYPE
import commons.createEmbed
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class Ping : Command() {

    companion object {
        var inputTime: LocalDateTime? = null
    }

    private fun getColorByPing(ping: Long): Color {
        if (ping < 100) return Color.green
        if (ping < 400) return Color.cyan
        if (ping < 700) return Color.yellow
        return if (ping < 1000) Color.orange else Color.red
    }

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val processing = ChronoUnit.MILLIS.between(inputTime, LocalDateTime.now())
        val ping = event.jda.gatewayPing
        event.textChannel.sendMessage(
            createEmbed("Ping", getColorByPing(ping), String.format(":ping_pong:   **Pong!**\n\n" +
                    "Der Bot braucht `%s` Millisekunden um zu antworten.\nEs dauert `%s` Millisekunden den Befehl zu parsen und der Ping ist `%s` Millisekunden.",
                processing + ping, processing, ping))).queue()
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Zeigt den Ping zum Bot an`"
    }

    override fun description(): String {
        return "Pong!"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.ESSENTIALS
    }
}