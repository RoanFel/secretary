package commands

import commons.CMDTYPE
import commons.getPREFIX
import core.PermCheck
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

abstract class Command {

    lateinit var command: String

    protected fun sendErrorMessage(tc: TextChannel) {
        tc.sendMessage(commons.error(tc.guild, help(tc.guild))).queue()
    }

    abstract fun action(args: Array<String>, event: MessageReceivedEvent)

    open fun help(guild: Guild): String {
        command = PermCheck.getPermPre(permission()) + "**" + getPREFIX(guild) + toString()
        return "**Verwendung: **\n" +
                command
    }

    abstract fun description(): String
    abstract fun commandType(): CMDTYPE

    open fun permission(): Int { return 0 }

    override fun toString(): String {
        return this.javaClass.simpleName.toLowerCase()
    }
}