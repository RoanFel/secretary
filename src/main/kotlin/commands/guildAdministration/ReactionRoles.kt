package commands.guildAdministration

import com.vdurmont.emoji.EmojiParser
import commands.Command
import commons.CMDTYPE
import commons.error
import commons.success
import data.getEmojiRoles
import data.removeEmojiRoles
import data.saveEmojiRoles
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class ReactionRoles : Command() {

    private fun getChannel(event: MessageReceivedEvent, chanId: String): TextChannel? {
        val chan = if(chanId.toLongOrNull() != null) {
            event.guild.getTextChannelById(chanId)
        } else {
            event.guild.getTextChannelsByName(chanId, true).firstOrNull()
        }

        if(chan == null) {
            event.textChannel.sendMessage(error(
                event.guild, "Es gibt keinen Channel mit der ID " +
                        "oder dem Namen $chanId")).queue()
        }
        return chan
    }

    private fun getMessage(event: MessageReceivedEvent, messageId: String, chan: TextChannel): Message? {
        val message = if(messageId.toLongOrNull() != null) {
            chan.retrieveMessageById(messageId).complete()
        } else {
            null
        }
        if(message == null) {
            event.textChannel.sendMessage(error(
                event.guild, "Es gibt keine Nachricht mit der ID $messageId im Channel ${chan.name}"
            )).queue()
        }
        return message
    }

    private fun getRole(event: MessageReceivedEvent, roleId: String): Role? {
        val role = if(roleId.toLongOrNull() != null) {
            event.guild.getRoleById(roleId)
        } else {
            event.guild.getRolesByName(roleId, true).firstOrNull()
        }

        if(role == null) {
            event.textChannel.sendMessage(error(
                event.guild, "Es gibt keine Rolle mit der ID " +
                        "oder dem Namen $roleId")).queue()
        }
        return role
    }

    private fun getEmoji(event: MessageReceivedEvent): String? {
        val merged = ArrayList<String>()
        val emojis = EmojiParser.extractEmojis(event.message.contentRaw)
        emojis.forEach { merged.add(EmojiParser.parseToUnicode(it)) }
        val emotes = event.message.emotes.map { it.name + ":" + it.id }
        merged.addAll(emotes)
        if (merged.isEmpty() || merged.size > 1) {
            event.channel.sendMessage(error(event.guild,"Es wurden kein oder mehr als ein Emoji übergeben." +
                    "Immer nur ein Emoji für den Befehl angeben")).queue()
            return null
        }
        return merged.first()
    }

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if(args.size < 3) {
            sendErrorMessage(event.textChannel)
            return
        }
        val action = args[0]
        if ((action != "add" && action != "remove") || (action == "add" && args.size < 5)) {
            sendErrorMessage(event.textChannel)
            return
        }

        val chanId = args[1]
        val messageId = args[2]
        val chan = getChannel(event, chanId) ?: return
        val message = getMessage(event, messageId, chan) ?: return

        if (action == "remove") {
            removeEmojiRoles(message.idLong)
            event.textChannel.sendMessage(success(event.guild, "Alle Rollenvergaben von der Nachricht " +
                    "mit der ID ${message.id} im Channel ${chan.name} entfernt.")).queue()
            return
        }

        val roleId = args[4]
        val role = getRole(event, roleId) ?: return
        val emoji = getEmoji(event) ?: return

        message.addReaction(emoji).queue()
        saveEmojiRoles(message.idLong, hashMapOf(Pair(emoji, role)))

        event.channel.sendMessage(success(event.guild, "Bei der Nachricht mit der ID ${message.id} " +
                "im Channel ${chan.asMention} wird bei einer Reaktion mit dem Emoji $emoji die Rolle ${role.name} " +
                "zum User hinzugefügt. Das Emoji wurde als Reaktion zur Nachricht hinzugefügt.")).queue()
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " add <ChanID> <MessageID> <Emoji> <Rolle>** - `Fügt der Nachricht mit der ID <MessageID> den <Emoji> als Reaktion hinzu, " +
                "und vergibt jedes mal wenn einer mit dem Emoji reagiert die <Rolle>`\n" +
                "$command remove <ChanID> <MessageID>** - `Entfernt alle Rollen die mit der Nachricht verknüpft sind`"
    }

    override fun description(): String {
        return "Setzt ein manuelles Rollenmenü für eine vorhandene Nachricht"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.GUILDADMIN
    }

    override fun permission(): Int {
        return 1
    }

}