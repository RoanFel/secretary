package commands.guildAdministration

import com.vdurmont.emoji.EmojiParser
import commands.Command
import commons.*
import data.removeChannelReactions
import data.saveReaction
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class React : Command() {

    private fun getChan(event: MessageReceivedEvent, id: String): TextChannel? {
        val chanId = id.toLongOrNull() ?: -1
        val chan = event.guild.getTextChannelById(chanId)
        if (chan == null) {
            event.channel.sendMessage(error(event.guild, "Es gibt keinen Channel mit der Id $id")).queue()
        }
        return chan
    }

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        if(args.size < 2) {
            sendErrorMessage(event.textChannel)
            return
        }
        if(args[0].toLowerCase() == "delete") {
            val chan = getChan(event, args[1]) ?: return
            removeChannelReactions(chan)
            event.channel.sendMessage(success(event.guild, "Automatische Reaktionen für den Channel " +
                    "${chan.name} mit der ID ${chan.id}  gelöscht")).queue()
            return
        }
        val chan = getChan(event, args[0]) ?: return
        val merged = ArrayList<String>()
        val emojis = EmojiParser.extractEmojis(event.message.contentRaw)
        emojis.forEach { merged.add(EmojiParser.parseToUnicode(it)) }
        val emotes = event.message.emotes.map { it.name + ":" + it.id }
        merged.addAll(emotes)
        if (merged.isEmpty()) {
            event.channel.sendMessage(error(event.guild,"Es wurden keine Emojis übergeben")).queue()
            return
        }
        var perm = args[args.lastIndex].toIntOrNull() ?: -1
        if(perm > 3) perm = -1
        merged.forEach { saveReaction(chan, it, perm) }
        event.channel.sendMessage(success(event.guild, "Automatische Reaktionen für den Channel " +
                "${chan.name} mit der ID ${chan.id} für die Emojis ${merged.joinToString(", ")} " +
                "${if(perm > -1) " und die Berechtigung $perm" else ""} erfolgreich gesetzt")).queue()
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " <ChanID> <Emojis> <Permlvl?>** - " +
                "```Setzt bei kommenden Nachrichten im angegebenen Channel die angegebenen Emojis\n" +
                "Optional Permlvl (nur bei Nachrichten von folgenden Rollen):\n" +
                "3 = Owner\n" +
                "2 = ${getPERMROLES2Names(guild).joinToString(", ")}\n" +
                "1 = ${getPERMROLES1Names(guild).joinToString(", ")}\n" +
                "0 = alle anderen```\n" +
                "$command delete <ChanID>** - `Löscht automatische Reaktionen vom angegebenen Channel`"
    }

    override fun description(): String {
        return "Setzt im übergebenen Channel bei allen Nachrichten die übergebenen Reaktionen " +
                "(ggf nur bei Mitgliedern einer bestimmten Berechtigung)"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.GUILDADMIN
    }

    override fun permission(): Int {
        return 1
    }

}