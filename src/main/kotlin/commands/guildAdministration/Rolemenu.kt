package commands.guildAdministration

import commands.Command
import commons.CMDTYPE
import commons.createEmbed
import listener.RoleMenuConfigurationListener
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color

class Rolemenu: Command() {
    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val message = event.channel.sendMessage(createEmbed("Rollenmenü", Color.MAGENTA,
                "Erstelle ein neues Rollenmenü oder bearbeite ein schon vorhandenes.\n\n" +
                        "Rollenmenüs dienen dazu das über Reaktionen automatisch Rollen zugeteilt werden.\n\n\n\n\n",
                fields = listOf(MessageEmbed.Field("Optionen",
                    "${RoleMenuConfigurationListener.emojis["a"]} - Neues Rollenmenü erstellen\n" +
                        "${RoleMenuConfigurationListener.emojis["b"]} - Rollenmenü bearbeiten", false)))).complete()
        message.addReaction(RoleMenuConfigurationListener.emojis["x"].orEmpty()).complete()
        message.addReaction(RoleMenuConfigurationListener.emojis["a"].orEmpty()).complete()
        message.addReaction(RoleMenuConfigurationListener.emojis["b"].orEmpty()).complete()
        message.addReaction(RoleMenuConfigurationListener.emojis["c"].orEmpty()).complete()
        message.addReaction(RoleMenuConfigurationListener.emojis["d"].orEmpty()).complete()
        message.addReaction(RoleMenuConfigurationListener.emojis["e"].orEmpty()).complete()
        message.addReaction(RoleMenuConfigurationListener.emojis["f"].orEmpty()).complete()
        message.addReaction(RoleMenuConfigurationListener.emojis["g"].orEmpty()).complete()
        RoleMenuConfigurationListener.rolemenus.add(RoleMenuConfigurationListener.Rolemenu(message, 0, RoleMenuConfigurationListener.deleteMessage(message)))
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + "** - `Öffnet ein Menü um ein Rollenmenü zu erstellen`"
    }

    override fun description(): String {
        return "Erstellt Rollenmenüs wo man über Reaktionen sich automatisch Rollen zuweisen/entfernen kann"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.GUILDADMIN
    }

    override fun permission(): Int {
        return 1
    }

}