package commands.guildAdministration

import commands.Command
import commons.*
import data.removeBlacklistEntry
import data.saveBlacklistEntry
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.awt.Color

class Blacklist : Command() {

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val blackList = getBLACKLIST(event.guild)
        val victim: Member
        if (args.isNotEmpty()) {
            victim = if (event.message.mentionedMembers.size > 0) event.message.mentionedMembers[0] else {
                event.textChannel.sendMessage(error(event.guild,
                    "Bitte einen existierendes User zur Blacklist hinzufügen")).queue()
                return
            }
            if (blackList.contains(victim)) {
                removeBlacklistEntry(event.guild, victim)
                event.textChannel.sendMessage(success(event.guild,
                    victim.asMention + " erfolgreich von der Blacklist entfernt.")).queue()
            } else {
                saveBlacklistEntry(event.guild, victim)
                event.textChannel.sendMessage(success(event.guild,
                    event.author.asMention + " hat " + victim.asMention + " zur Blacklist hinzugefügt.")).queue()
            }
            println(blackList.size)
        } else {
            val out = getBLACKLISTNames(event.guild).joinToString("\n")
            event.textChannel.sendMessage(createEmbed("**Blacklist**",Color.CYAN,
                "Mitglieder auf der Blacklist:\n\n$out")).queue()
        }
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " <@User>** - `Setzt/Löscht <User> auf der Blacklist`"
    }

    override fun description(): String {
        return "Entzieht Mitgliedern die Erlaubnis den Bot zu nutzen"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.GUILDADMIN
    }

    override fun permission(): Int {
        return 2
    }

    companion object {
        fun check(user: User, guild: Guild): Boolean {
            val member = guild.getMemberById(user.idLong) ?: return false
            val blackList = getBLACKLIST(guild)
            if (blackList.contains(member)) {
                member.user.openPrivateChannel().complete().sendMessage(error(guild,
                    "Momentan ist es dir nicht erlaubt den Bot zu verwenden.\n" +
                            "Benachrichtige den Support oder den Server-Owner um dich von der Blacklist entfernen zu lassen")
                ).queue()
                return false
            }
            return true
        }
    }
}
