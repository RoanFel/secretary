package commands.guildAdministration

import commands.Command
import commons.CMDTYPE
import commons.error
import commons.success
import core.Secretary
import data.Autochan
import data.Autochans
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.VoiceChannel
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.Serializable


class Autochannel : Command(), Serializable {
    private fun setChan(id: String, g: Guild, tc: TextChannel, autochans: HashMap<VoiceChannel, Pair<Guild, Boolean>>, boolean: Boolean) {
        val idLong = id.toLongOrNull() ?: -1
        val vc = getVchan(idLong, g)
        when {
            vc == null -> {
                tc.sendMessage(error(g, "Voice channel mit der ID `${id}` existiert nicht.")).queue()
            }
            autochans.containsKey(vc) -> {
                tc.sendMessage(error(g, "Der Voicechannel `${vc.name}` ist bereits als " +
                        "${if(boolean) "Gamesautochannel" else "Autochannel"} gesetzt.")).queue()
            }
            else -> {
                saveVchan(vc, g, boolean)
                tc.sendMessage(success(g,"Erfolgreich Voicechannel `${vc.name}` als " +
                        "${if(boolean) "Gamesautochannel" else "Autochannel"} gesetzt.")).queue()
            }
        }
    }

    private fun unsetChan(id: String, g: Guild, tc: TextChannel, autochans: HashMap<VoiceChannel, Pair<Guild, Boolean>>) {
        val idLong = id.toLongOrNull() ?: -1
        val vc = getVchan(idLong, g)
        when {
            vc == null -> {
                tc.sendMessage(error(g, "Voice channel mit der ID `${id}` existiert nicht.")).queue()
            }
            !autochans.containsKey(vc) -> {
                tc.sendMessage(error(g, "Voice channel `${vc.name}` ist kein Autochannel.")).queue()
            }
            else -> {
                unsetChan(vc)
                tc.sendMessage(success(g, "Erfolgreich Voicechannel `${vc.name}` als " +
                        "${if(autochans[vc]!!.second) "Gamesautochannel" else "Autochannel" } entfernt.")).queue()
            }
        }
    }

    private fun listChans(guild: Guild, tc: TextChannel, autochans: HashMap<VoiceChannel, Pair<Guild, Boolean>>) {
        val sb = StringBuilder().append("**Autochannel**\n\n")
        autochans.keys
                .filter { autochans[it]?.first == (guild) }
                .forEach { sb.append(":white_small_square:   `${it.name}` *(${it.id})*  " +
                        "(${if(autochans[it]!!.second) "Gamesautochannel" else ""})\n") }
        tc.sendMessage(success(guild, sb.toString())).queue()
    }

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val autochans = load(Secretary.jda)
        val g: Guild = event.guild
        val tc = event.textChannel
        if (args.isEmpty()) {
            sendErrorMessage(tc)
            return
        }
        when (args[0].toLowerCase()) {
            "list", "show" -> listChans(g, tc, autochans)
            "add", "set" -> if (args.size < 2) sendErrorMessage(tc) else setChan(args[1], g, tc, autochans, false)
            "setasgame" -> if (args.size < 2) sendErrorMessage(tc) else setChan(args[1], g, tc, autochans, true)
            "remove", "delete", "unset" -> if (args.size < 2) sendErrorMessage(tc) else unsetChan(args[1], g, tc, autochans)
            else -> sendErrorMessage(tc)
        }
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " set <Chan ID>** - `Setzt einen Voicechannel als Autochannel`\n" +
                "$command setAsGame <Chan ID>** - `Setzt einen Voicechannel als Gamesautochannel`\n" +
                "$command unset <Chan ID>** - `Löscht einen Voicechannel als Autochannel`\n" +
                "$command list** - `Zeigt alle registrierten Autochannel an`\n"
    }

    override fun description(): String {
        return "Verwaltet die Autochannel Funktion"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.GUILDADMIN
    }

    override fun permission(): Int {
        return 1
    }

    companion object {

        fun unsetChan(vc: VoiceChannel) {
            transaction {
                val autochan = Autochan.find { (Autochans.chanId eq vc.idLong) and (Autochans.guildId eq vc.guild.idLong) }
                autochan.forEach { it.delete() }
            }
        }

        fun saveVchan(vc: VoiceChannel, g: Guild, boolean: Boolean) {
            transaction {
                Autochan.new {
                    chanId = vc.idLong
                    guildId = g.idLong
                    gameschan = boolean
                }
            }
        }

        private fun getVchan(id: Long, g: Guild): VoiceChannel? {
            return g.getVoiceChannelById(id)
        }

        private fun getGuild(id: Long, jda: JDA): Guild? {
            return jda.getGuildById(id)
        }

        fun load(jda: JDA): HashMap<VoiceChannel, Pair<Guild, Boolean>> {
            val autochans: HashMap<VoiceChannel, Pair<Guild, Boolean>> = HashMap()

            transaction {
                val chans = Autochan.all()
                chans.forEach lit@{
                    val gId = it.guildId
                    val g = getGuild(gId, jda)
                    if (g == null) {
                        it.delete()
                        return@lit
                    }
                    val cId = it.chanId
                    val c = getVchan(cId, g)
                    if (c == null) {
                        it.delete()
                        return@lit
                    }
                    autochans[c] = Pair(g, it.gameschan)
                }
            }
            return autochans
        }
    }
}