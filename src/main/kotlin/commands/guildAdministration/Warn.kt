package commands.guildAdministration

import commands.Command
import commons.*
import data.getCurrentUserWarns
import data.getUserWarnByWarnId
import data.saveUserWarn
import data.setNewEnddate
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.awt.Color

class Warn : Command() {

    private fun getUser(userId: String, event: MessageReceivedEvent): User? {
        val user = if(userId.toLongOrNull() != null) {
            event.jda.getUserById(userId)
        } else {
            event.message.mentionedUsers.firstOrNull()
        }

        if(user == null) {
            event.textChannel.sendMessage(error(event.guild, "Es gibt keinen User mit der ID " +
                    "oder dem Namen $userId")).queue()
            return null
        }
        return user
    }

    private fun getPoints(points: String, event: MessageReceivedEvent): Long? {
        if(points.toLongOrNull() == null) {
            event.textChannel.sendMessage(error(event.guild, "$points ist keine zulässige Punktzahl. Bitte eine Zahl zwischen 1-30 übergeben.")).queue()
            return null
        }
        val longPoints = points.toLong()
        if (longPoints < 1 || longPoints > 30) {
            event.textChannel.sendMessage((error(event.guild, "$points ist keine zulässige Punktzahl. Bitte eine Zahl zwischen 1-30 übergeben."))).queue()
        }
        return points.toLong()
    }

    private fun getDuration(points: Long): Int {
        return when(points) {
            1L -> 1
            2L -> 2
            3L -> 3
            4L -> 5
            5L -> 8
            6L -> 13
            7L -> 21
            8L -> 34
            9L -> 55
            10L -> 89
            11L -> 123
            12L -> 157
            13L -> 191
            14L -> 225
            15L -> 280
            16L -> 335
            17L -> 390
            18L -> 445
            19L -> 500
            20L -> 589
            21L -> 712
            22L -> 903
            23L -> 1183
            24L -> 1573
            25L -> 2073
            26L -> 2785
            27L -> 3968
            28L -> 6041
            29L -> 10009
            in 30L..Long.MAX_VALUE -> 20018
            else -> 0
        }
    }

    override fun action(args: Array<String>, event: MessageReceivedEvent) {
        val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")
        val tc = event.textChannel
        val guild = event.guild

        if(args.size == 2) {
            if(args[0].toLowerCase() == "remove") {
                val warn = getUserWarnByWarnId(args[1])
                if(warn == null) {
                    tc.sendMessage(error(guild, "Es gibt keinen Warn mit der ID ${args[1]}")).queue()
                } else {
                    transaction { warn.delete() }
                    tc.sendMessage(success(guild, "Der Warn mit der ID ${args[1]} für den User mit der ID ${warn.userId} wurde gelöscht.")).queue()
                }
                return
            }
        }

        if(args.size < 3) {
            sendErrorMessage(tc)
            return
        }

        val user = getUser(args[0], event) ?: return
        val points = getPoints(args[1], event) ?: return
        val reason = args[2]

        val duration = getDuration(points)
        val enddate = DateTime.now().plusDays(duration)
        val date = setNewEnddate(guild.idLong, user.idLong, enddate)
        val member = guild.getMember(user)

        val warnpointsold = getCurrentUserWarns(guild.idLong, user.idLong).map { it.points }.sum()
        val warnpointnew = warnpointsold + points

        user.openPrivateChannel().complete().sendMessage(createEmbed("Verwarnung", Color.RED,
            "Du wurdest aus dem folgendn Grund verwarnt: \n\n$reason\n\n" +
                    "Dafür hast du $points Warnpunkte bekommen.\n\n" +
                    "Die Verwarnung läuft bis zum ${formatter.print(date)}",
            getAuthor(guild), guild.iconUrl, getServerFooter(guild))).queue()

        var strafe = false
        var kick = false
        var ban = false

        if(member != null) {
            if(warnpointnew >= 30) {
                guild.ban(user, 7, reason).queue()
                ban = true
            } else if (warnpointnew >= 20 && warnpointsold < 20) {
                kick = true
                guild.kick(member, reason).queue()
            } else if(warnpointnew >= 10 && warnpointsold < 10) {
                strafe = true
                val roles = member.roles
                guild.modifyMemberRoles(member, null, roles).queue()
            }
        }

        val text = when(warnpointnew) {
            in 1..9 -> "Noch ${10-warnpointnew} bis zur Strafe, ${20-warnpointnew} bis zum Kick und ${30-warnpointnew} bis zum Ban."
            in 10..19 -> {
                val x = if (strafe) "Dir wurden zur Strafe alle Rollen abgezogen. Diese musst du neu dir zuweisen, bzw neu verdienen.\n" else ""
                x + "Noch ${20-warnpointnew} bis zum Kick und ${30-warnpointnew} bis zum Ban."
            }
            in 20..29 -> {
                val x = if (kick) "Du wurdest zur Strafe gekickt.\n" else ""
                x + "Noch ${30-warnpointnew} bis zum Ban."
            }
            in 30..Long.MAX_VALUE -> "Du wurdest zur Strafe gebannt. Der Ban läuft bis deine Verwarnungen abgelaufen sind."
            else -> ""
        }

        guild.getTextChannelById(getWarnlogchanId(guild))?.sendMessage(createEmbed("Verwarnung!", Color.RED,
            "Der User ${user.asMention} mit der ID ${user.id} wurde mit $points Punkten verwarnt!\n" +
                    "Grund: $reason\n" +
                    "Alle Verwarnungen des Users laufen jetzt bis zum ${formatter.print(date)}\n\n" +
                    text, getAuthor(guild), guild.iconUrl, getServerFooter(guild)))?.queue()

        saveUserWarn(guild.idLong, user.idLong, points, reason, date, ban)

        event.message.delete().queue()
    }

    override fun description(): String {
        return "Zum verwarnen von Usern mit Punkten. Bei 10 Punkten verliert der User alle seine Rollen und muss diese " +
                "sich erneut zuweisen bzw. verdienen. Bei 20 Punkten wird der User gekickt und bei 30 Punkten gebannt.\n" +
                "Die Verwarnungen laufen abhängig von der Punktzahl (je mehr Punkte desto länger läuft die Verwarnung)"
    }

    override fun commandType(): CMDTYPE {
        return CMDTYPE.GUILDADMIN
    }

    override fun help(guild: Guild): String {
        return super.help(guild) + " <UserID/@User> <Punkte> <Grund>** - ```Verwarnt <User> mit <Punkte> Punkten.\n" +
                "Dem User wird eine Nachricht über die Verwarnung mit den Punkten und dem Grund geschickt.\n" +
                "Außerdem wird die Verwarnung in den warnlog geschickt wenn einer gesetzt wurde.```\n" +
                "$command remove <WarnID>** - `Entfernt den Warn <WarnID> (die ID steht dabei wenn man !warns von einem User abfragt)`"
    }

    override fun permission(): Int {
        return 1
    }

}