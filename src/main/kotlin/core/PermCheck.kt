package core

import commons.STATICS
import commons.getPERMROLES1
import commons.getPERMROLES2
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

object PermCheck {
    private fun isHost(user: User): Boolean {
        return user.id.toLong() == STATICS.BOT_OWNER_ID
    }

    fun getLvl(member: Member?): Int {
        if (member == null) return 0
        if (isHost(member.user)) return 4
        if (member == member.guild.owner) return 3
        val perm2 = getPERMROLES2(member.guild)
        if(perm2.isNotEmpty()) {
            if (member.roles.any { perm2.contains(it) }) return 2
        } else {
            if (member.roles.any { it.hasPermission(Permission.ADMINISTRATOR)}) return 2
        }
        val perm1 = getPERMROLES1(member.guild)
        if(perm1.isNotEmpty()) {
            if (member.roles.any { perm1.contains(it)}) return 1
        } else {
            if (member.roles.any { it.hasPermission(Permission.MANAGE_CHANNEL)}) return 1
        }
        return 0
    }

    fun check(required: Int, event: MessageReceivedEvent): Boolean {
        return required > getLvl(event.member)
    }

    fun sendPermMessage(required: Int, event: MessageReceivedEvent) {
        event.textChannel.sendMessage(commons.error(event.guild, "Du benötigst Berechtigungslevel `" + required +
                    "` oder höher!\n(Dein aktueller Berechtigungslevel ist `" + getLvl(event.member!!) + "`).")).queue()
    }

    fun getPermPre(lvl: Int): String {
        return when (lvl) {
            1 -> ":small_blue_diamond:  "
            2 -> ":small_orange_diamond:  "
            3 -> ":small_red_triangle_down:  "
            else -> ":white_small_square:  "
        }
    }
}