package core

import commons.STATICS
import org.json.JSONObject
import java.io.*
import java.util.stream.Collectors
import kotlin.system.exitProcess

private fun initialize() {
    val main = JSONObject()
    val mysql = JSONObject()
            .put("host", "")
            .put("port", "3306")
            .put("username", "")
            .put("password", "")
            .put("database", "secretarybot")
    main.put("token", "")
            .put("prefix", STATICS.PREFIX)
            .put("ownerid", "")
            .put("mysql", mysql)
    try {
        val br = BufferedWriter(FileWriter("SETTINGS.json"))
        br.write(main.toString())
        br.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

@Throws(IOException::class)
fun loadSettings() {
    if (!File("SETTINGS.json").exists()) {
        initialize()
        println("Please open \"SETTINGS.json\" file and enter your discord token and owner ID there!")
        exitProcess(-1)
    } else {
        val br = BufferedReader(FileReader("SETTINGS.json"))
        val out = br.lines().collect(Collectors.joining("\n"))
        val obj = JSONObject(out)
        STATICS.TOKEN = obj.getString("token")
        STATICS.BOT_OWNER_ID = obj.getString("ownerid")!!.toLong()
        val mysql = obj.getJSONObject("mysql")
        STATICS.SQL_HOST = mysql.getString("host")
        STATICS.SQL_PORT = mysql.getString("port")
        STATICS.SQL_USER = mysql.getString("username")
        STATICS.SQL_PASS = mysql.getString("password")
        STATICS.SQL_DB = mysql.getString("database")
    }
}
