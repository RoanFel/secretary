package core

import commands.Command
import commands.chat.Clear
import commands.chat.Orakel
import commands.essentials.*
import commands.etc.BotStats
import commands.guildAdministration.*
import commands.settings.*
import commons.*
import commons.STATICS.ACTIVITY
import commons.STATICS.SQL_DB
import commons.STATICS.SQL_HOST
import commons.STATICS.SQL_PASS
import commons.STATICS.SQL_PORT
import commons.STATICS.SQL_USER
import commons.STATICS.TOKEN
import data.Database
import listener.*
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.OnlineStatus
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.requests.GatewayIntent
import java.awt.Color
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.util.*
import javax.security.auth.login.LoginException

fun main(args: Array<String>) {
    Secretary.args = args
    loadSettings()
    Database(SQL_HOST, SQL_PORT, SQL_USER, SQL_PASS, SQL_DB).initialize()
    BotStats.load()
    Secretary.builder = JDABuilder.create(TOKEN, mutableListOf(GatewayIntent.GUILD_EMOJIS, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_BANS, GatewayIntent.GUILD_VOICE_STATES,
            GatewayIntent.GUILD_PRESENCES, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MESSAGE_REACTIONS, GatewayIntent.DIRECT_MESSAGES))
        .setAutoReconnect(true)
        .setStatus(OnlineStatus.ONLINE)
        .setActivity(ACTIVITY)
    Secretary.init()
    try {
        Secretary.jda = Secretary.builder.build()
    } catch (e: LoginException) {
        e.printStackTrace()
    }
}

object Secretary {
    lateinit var args: Array<String>
    lateinit var builder: JDABuilder
    var commands = HashMap<String, Command>()
    lateinit var jda: JDA


    fun init() {
        initializeCommands()
        initializeListeners()
    }

    private fun initializeCommands() {
        commands["ping"] = Ping()
        commands["botstats"] = BotStats()
        commands["blacklist"] = Blacklist()
        commands["help"] = Help()
        commands["id"] = Id()
        commands["info"] = Info()
        commands["stats"] = Stats()
        commands["userinfo"] = UserInfo()
        commands["autochannel"] = Autochannel()
        commands["clear"] = Clear()
        commands["botchannels"] = BotChannels()
        commands["react"] = React()
        commands["rolemenu"] = Rolemenu()
        commands["warn"] = Warn()
        commands["warns"] = Warns()
        commands["permlvl"] = PermLvl()
        commands["prefix"] = Prefix()
        commands["settings"] = Settings()
        commands["msg"] = Msg()
        commands["dmmsg"] = DMMsg()
        commands["reactionroles"] = ReactionRoles()
        commands["log"] = Log()
        commands["orakel"] = Orakel()
    }

    private fun initializeListeners() {
        builder.addEventListeners(BotListener(), AutochannelHandler(), ReadyListener(),
                BotGuildJoinListener(), PrivateMessageListener(), VoiceChannelListener(), StatsListener(),
                RoleMenuConfigurationListener(), RoleMenuListener(), GuiltMemberListener(), LogListener())
    }

    fun handleCommand(cmd: CommandContainer) {
        val commandString = cmd.invoke.toLowerCase()
        val command = commands[commandString] ?: return
        BotStats.incrementCommandsExecuted()
        if (!PermCheck.check(command.permission(), cmd.event)) {
            command.action(cmd.args, cmd.event)
        } else {
            PermCheck.sendPermMessage(command.permission(), cmd.event)
        }

        val event = cmd.event
        val chan = event.guild.getTextChannelById(getCmdchanId(cmd.event.guild)) ?: return
        val author = event.author
        val eb = createEmbed("Befehl erhalten", Color.ORANGE, "```" + event.message.contentRaw + "```",
            author.name + "#" + author.discriminator, author.effectiveAvatarUrl, getServerFooter(event.guild))
        chan.sendMessage(eb).queue()
        if(cmd.invoke.toLowerCase() != "orakel"){
            deleteMessage(event.message, 1000)
        }


        val list = ArrayList<String>()
        list.add(event.guild.id)
        list.add(getCurrentSystemTime())
        list.add(event.member?.effectiveName ?: "")
        list.add(event.message.contentRaw)
        addToLogfile(event)
    }



    private fun addToLogfile(e: MessageReceivedEvent) {
        val logFile = File("CMDLOG.txt")
        val bw = BufferedWriter(FileWriter(logFile, true))
        if (!logFile.exists()) logFile.createNewFile()
        bw.write(java.lang.String.format("%s [%s (%s)] [%s (%s)] '%s'\n",
            getCurrentSystemTime(),
            e.guild.name,
            e.guild.id,
            e.author.name,
            e.author.id,
            e.message.contentRaw))
        bw.close()
    }
}
