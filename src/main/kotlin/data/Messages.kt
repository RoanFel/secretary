package data

import net.dv8tion.jda.api.entities.Message
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.concurrent.schedule

private val logger = LoggerFactory.getLogger("MessageCache")


object Messages : IntIdTable("messages") {
    val authorId = long("authorid")
    val chanId = long("chanid")
    val guildId = long("guildid")
    val messageId = long("messageid").uniqueIndex()
    val message = text("message")
    val createDate = datetime("createdate")
}

class MessageCache(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, MessageCache>(Messages)

    var authorId by Messages.authorId
    var chanId by Messages.chanId
    var guildId by Messages.guildId
    var messageId by Messages.messageId
    var message by Messages.message
    var createDate by Messages.createDate
}

fun saveMessage(message: Message) {
    transaction {
        val list = MessageCache.find { Messages.messageId eq message.idLong }.toList()
        if(list.isNotEmpty()) return@transaction
        val date = message.timeCreated
        val zone = DateTimeZone.forOffsetMillis(date.offset.totalSeconds*1000)
        MessageCache.new {
            authorId = message.author.idLong
            chanId = message.channel.idLong
            guildId = message.guild.idLong
            messageId = message.idLong
            this.message = message.contentDisplay
            createDate = DateTime(date.year, date.monthValue, date.dayOfMonth, date.hour, date.minute, date.second, zone)
        }
    }
}

fun deleteMessage(messageId: Long) {
    transaction {
        MessageCache.find { Messages.messageId eq messageId }.forEach { it.delete() }
    }
}

fun updateMessage(message: Message) {
    transaction {
        val m = MessageCache.find { Messages.messageId eq message.idLong }.firstOrNull()
        if (m != null) {
            m.message = message.contentDisplay
        }
    }
}

fun getMessage(messageId: Long): MessageCache? {
    return transaction {
        MessageCache.find { Messages.messageId eq messageId }.firstOrNull()
    }
}

fun deleteMessages() {
    transaction {
        MessageCache.find { Messages.createDate.less(DateTime.now().minusMonths(1)) }.forEach { it.delete() }
    }
}

fun messageCacheScheduler() {
    Timer().schedule(0, 86400000) {
        logger.info("Delete Messages older then 1 Month")
        deleteMessages()
    }
}