package data

import net.dv8tion.jda.api.entities.Role
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

object EmojiRoles : IntIdTable("emojiroles") {
    val messageId = long("messageid")
    val emoji = binary("emoji", 255)
    val roleId = long("roleid")
}

class EmojiRole(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, EmojiRole>(EmojiRoles)

    var messageId by EmojiRoles.messageId
    var emoji by EmojiRoles.emoji
    var roleId by EmojiRoles.roleId
}

fun saveEmojiRoles(messageId: Long, emojiRoles: HashMap<String, Role>) {
    transaction {
        emojiRoles.forEach {
            EmojiRole.new {
                this.messageId = messageId
                this.emoji = it.key.toByteArray()
                this.roleId = it.value.idLong
            }
        }
    }
}

fun getEmojiRoles(messageId: Long): List<EmojiRole> {
    return transaction {
        EmojiRole.find { EmojiRoles.messageId eq messageId }.toList()
    }
}

fun removeEmojiRoles(messageId: Long) {
    transaction {
        EmojiRole.find { EmojiRoles.messageId eq messageId }.forEach { it.delete() }
    }
}