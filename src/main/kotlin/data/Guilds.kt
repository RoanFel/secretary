package data

import commons.STATICS
import net.dv8tion.jda.api.entities.Guild
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

object Guilds : IntIdTable("guilds") {
    val guildId = long("guildid").uniqueIndex()
    val prefix = varchar("prefix", 255)
    val joinmsg = text("joinmsg")
    val leavemsg = text("leavemsg")
    val dmjoinmsg = text("dmjoinmsg")
    val dmleavemsg = text("dmleavemsg")
    val cmdlogchanId = long("cmdlogchanid")
    val voicelogchanId = long("voicelogchanid")
    val supportchanId = long("supportchanid")
    val logchanId = long("logchanid")
    val logDelete = bool("logdelete")
    val logEdit = bool("logedit")
    val feedbackchanId = long("feedbackchanid")
    val warnlogchanId = long("warnlogchanid")
    val joinmsgchanId = long("joinmsgchanid")
    val leavemsgchanId = long("leavemsgchanid")
    val statscategoryId = long("statscategoryid")
    val statsmembersId = long("statsmembersid")
    val statsmembersonlyId = long("statsmembersonlyid")
    val statsbotsId = long("statsbotsid")
    val statsonlineId = long("statsonlineid")
    val statsvoiceId = long("statsvoiceid")
    val privatvoiceId = long("privatvoiceid")
}

class GuildDB(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, GuildDB>(Guilds)

    var guildId by Guilds.guildId
    var prefix by Guilds.prefix
    var joinmsg by Guilds.joinmsg
    var leavemsg by Guilds.leavemsg
    var dmjoinmsg by Guilds.dmjoinmsg
    var dmleavemsg by Guilds.dmleavemsg
    var cmdlogchanId by Guilds.cmdlogchanId
    var voicelogchanId by Guilds.voicelogchanId
    var supportchanId by Guilds.supportchanId
    var logchanId by Guilds.logchanId
    var logDelete by Guilds.logDelete
    var logEdit by Guilds.logEdit
    var feedbackchanId by Guilds.feedbackchanId
    var warnlogchanId by Guilds.warnlogchanId
    var joinmsgchanId by Guilds.joinmsgchanId
    var leavemsgchanId by Guilds.leavemsgchanId
    var statscategoryId by Guilds.statscategoryId
    var statsmembersId by Guilds.statsmembersId
    var statsmembersonlyId by Guilds.statsmembersonlyId
    var statsbotsId by Guilds.statsbotsId
    var statsonlineId by Guilds.statsonlineId
    var statsvoiceId by Guilds.statsvoiceId
    var privatvoiceId by Guilds.privatvoiceId
}

fun saveGuild(id: Long) {
    transaction {
        GuildDB.new {
            guildId = id
            prefix = STATICS.PREFIX
            joinmsg = ""
            leavemsg = ""
            dmjoinmsg = ""
            dmleavemsg = ""
            cmdlogchanId = -1
            voicelogchanId = -1
            supportchanId = -1
            logchanId = -1
            logDelete = false
            logEdit = false
            feedbackchanId = -1
            warnlogchanId = -1
            joinmsgchanId = -1
            leavemsgchanId = -1
            statscategoryId = -1
            statsmembersId = -1
            statsmembersonlyId = -1
            statsbotsId = -1
            statsonlineId = -1
            statsvoiceId = -1
            privatvoiceId = -1
        }
    }
}

fun getGuild(guild: Guild): GuildDB {
    val guilds = transaction {
        GuildDB.find { Guilds.guildId eq guild.idLong }.toList()
    }

    if (guilds.count() != 1) {
        throw Error("Server not in Database!")
    } else {
        return guilds.first()
    }
}