package data

import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.VoiceChannel
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

object Gameschans : IntIdTable("gamechans") {
    val chanId = long("chanid")
    val guildId = long("guildid")
    val memberId = long("memberid")
}

class Gameschan(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Gameschan>(Gameschans)

    var chanId by Gameschans.chanId
    var guildId by Gameschans.guildId
    var memberId by Gameschans.memberId
}

fun saveGamesChan(vc: VoiceChannel, m: Member? = null) {
    transaction {
        Gameschan.new {
            chanId = vc.idLong
            guildId = vc.guild.idLong
            memberId = m?.idLong ?: -1
        }
    }
}

fun getGamesChanByMember(m: Member): List<Gameschan> {
    return transaction {
        Gameschan.find { Gameschans.memberId eq m.idLong }.toList()
    }
}