package data

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.not
import org.jetbrains.exposed.sql.transactions.transaction

object Perms : IntIdTable("perms") {
    val guildId = long("guildid")
    val roleId = long("roleid")
    val fullperm = bool("fullperm")
}

class Perm(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Perm>(Perms)

    var guildId by Perms.guildId
    var roleId by Perms.roleId
    var fullperm by Perms.fullperm
}

fun savePerm (guildId: Long, roleId: Long) {
    transaction {
        Perm.new {
            this.guildId = guildId
            this.roleId = roleId
            this.fullperm = false
        }
    }
}

fun getPerms (guildId: Long): List<Perm> {
    return transaction {
        Perm.find { (Perms.guildId eq guildId) and not(Perms.fullperm) }.toList()
    }
}

fun removePerm (guildId: Long, roleId: Long) {
    transaction {
        Perm.find { Perms.guildId eq guildId and (Perms.roleId eq  roleId) and not(Perms.fullperm) }.forEach { it.delete() }
    }
}

fun saveFullperm (guildId: Long, roleId: Long) {
    transaction {
        Perm.new {
            this.guildId = guildId
            this.roleId = roleId
            this.fullperm = true
        }
    }
}

fun getFullperms (guildId: Long): List<Perm> {
    return transaction {
        Perm.find { Perms.guildId eq guildId and Perms.fullperm }.toList()
    }
}

fun removeFullperm (guildId: Long, roleId: Long) {
    transaction {
        Perm.find { Perms.guildId eq guildId and (Perms.roleId eq  roleId) and Perms.fullperm }.forEach { it.delete() }
    }
}