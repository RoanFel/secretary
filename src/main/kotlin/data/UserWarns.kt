package data

import core.Secretary
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.jodatime.date
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.concurrent.schedule


private val logger = LoggerFactory.getLogger("UserWarns")

object UserWarns : IntIdTable("userwarns") {
    val guildId = long("guildid")
    val userId = long("userid")
    val points = long("points")
    val reason = text("reason")
    val enddate = date("enddate")
    val warnId = varchar("warnid", 255).uniqueIndex()
    val ban = bool("ban")
}

class UserWarn(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, UserWarn>(UserWarns)

    var guildId by UserWarns.guildId
    var userId by UserWarns.userId
    var points by UserWarns.points
    var reason by UserWarns.reason
    var enddate by UserWarns.enddate
    var warnId by UserWarns.warnId
    var ban by UserWarns.ban

}

fun saveUserWarn(guildId: Long, userId: Long, points: Long, reason: String, enddate: DateTime, ban: Boolean) {
    transaction {
        UserWarn.new {
            this.guildId = guildId
            this.userId = userId
            this.points = points
            this.reason = reason
            this.enddate = enddate
            this.warnId = UUID.randomUUID().toString()
            this.ban = ban
        }
    }
}

fun getUserWarnByWarnId(warnId: String): UserWarn? {
    return transaction {
        UserWarn.find { UserWarns.warnId eq warnId } .firstOrNull()
    }
}

fun getUserWarns(guildId: Long, userId: Long): List<UserWarn> {
    return transaction {
        UserWarn.find { UserWarns.guildId eq guildId and (UserWarns.userId eq userId) }.toList()
    }
}

fun getCurrentUserWarns(guildId: Long, userId: Long): List<UserWarn> {
    return transaction {
        UserWarn.find { UserWarns.guildId eq guildId and (UserWarns.userId eq userId) and
                (UserWarns.enddate.greaterEq(DateTime.now())) }.toList()
    }
}

fun setNewEnddate(guildId: Long, userId: Long, enddate: DateTime): DateTime {
    return transaction {
        val warns = UserWarn.find { UserWarns.guildId eq guildId and (UserWarns.userId eq userId) and
                (UserWarns.enddate.greaterEq(DateTime.now())) }.toList()
        val dates = warns.map { it.enddate }.toMutableList()
        dates.add(enddate)
        val newenddate = dates.max() ?: enddate
        warns.forEach { it.enddate = newenddate }
        newenddate
    }
}

fun deleteBans() {
    transaction {
        val userwarns = UserWarn.find { UserWarns.enddate.less(DateTime.now()) and UserWarns.ban }.toMutableList()
        userwarns.forEach {
            if(UserWarn.find { UserWarns.guildId eq it.guildId and (UserWarns.userId eq it.userId) and UserWarns.enddate.greaterEq(
                    DateTime.now()) }.sumBy { s -> s.points.toInt() } < 30) {
                val user = Secretary.jda.getUserById(it.userId)
                if(user == null) {
                    UserWarn.find { UserWarns.userId eq it.userId }.forEach { u -> u.delete() }
                    return@forEach
                }
                val guild = Secretary.jda.getGuildById(it.guildId)
                if(guild == null) {
                    UserWarn.find { UserWarns.guildId eq it.guildId }.forEach { u -> u.delete() }
                    return@forEach
                }
                guild.unban(user).queue()
                it.ban = false
            }
        }

    }
}

fun warnScheduler() {
    Timer().schedule(0, 86400000) {
        logger.info("Prove Warns and Delete unnecessary Bans")
        deleteBans()
    }
}