package data

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

object BlacklistEntrys : IntIdTable("blacklist") {
    val guildId = long("guildid")
    val userId = long("userid")
}

class BlacklistEntry(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, BlacklistEntry>(BlacklistEntrys)

    var guildId by BlacklistEntrys.guildId
    var userId by BlacklistEntrys.userId
}

fun saveBlacklistEntry(guild: Guild, user: Member) {
    transaction {
        BlacklistEntry.new {
            guildId = guild.idLong
            userId = user.idLong
        }
    }
}

fun removeBlacklistEntry(guild: Guild, user: Member) {
    transaction {
        BlacklistEntry.find { (BlacklistEntrys.guildId eq guild.idLong) and (BlacklistEntrys.userId eq user.idLong) }.forEach {
            it.delete()
        }
    }
}

fun getBlacklistEntrys(guildId: Long): List<BlacklistEntry> {
    return transaction {
        BlacklistEntry.find { (BlacklistEntrys.guildId eq guildId) }.toList()
    }
}