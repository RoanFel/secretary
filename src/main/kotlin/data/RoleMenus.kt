package data

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

object RoleMenus : IntIdTable("rolemenus") {
    val messageId = long("messageid")
    val chanId = long("chanid")
    val title = text("title")
    val description = text("description")
    val remove = bool("remove")
    val multiple = bool("multiple")
}

class RoleMenu(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, RoleMenu>(RoleMenus)

    var messageId by RoleMenus.messageId
    var chanId by RoleMenus.chanId
    var title by RoleMenus.title
    var description by RoleMenus.description
    var remove by RoleMenus.remove
    var multiple by RoleMenus.multiple
}

fun saveRolemenu(messageId: Long, chanId: Long, title: String, description: String, remove: Boolean, multiple: Boolean) {
    transaction {
        RoleMenu.new {
            this.messageId = messageId
            this.chanId = chanId
            this.title = title
            this.description = description
            this.remove = remove
            this.multiple = multiple
        }
    }
}

fun getRolemenu(messageId: Long?): RoleMenu? {
    if(messageId == null) return null
    return transaction {
        RoleMenu.find { RoleMenus.messageId eq messageId }.firstOrNull()
    }
}

fun deleteRolemenu(messageId: Long) {
    transaction {
        RoleMenu.find { RoleMenus.messageId eq messageId }.forEach { it.delete() }
    }
}