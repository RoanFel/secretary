package data

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

object UserGuildSupports : IntIdTable("user_guild_supports") {
    val userId = long("userid")
    val guildId = long("guildid")
}

class UserGuildSupport(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, UserGuildSupport>(UserGuildSupports)

    var userId by UserGuildSupports.userId
    var guildId by UserGuildSupports.guildId
}

fun saveUserGuild(userId: Long, guildId: Long) {
    transaction {
        UserGuildSupport.new {
            this.userId = userId
            this.guildId = guildId
        }
    }
}