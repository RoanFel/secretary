package data

import net.dv8tion.jda.api.entities.TextChannel
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

object Reactions : IntIdTable("reactions") {
    val chanId = long("chanid")
    val emoji = binary("emoji", 255)
    val perm = integer("perm")
}

class Reaction(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Reaction>(Reactions)

    var chanId by Reactions.chanId
    var emoji by Reactions.emoji
    var perm by Reactions.perm
}

fun saveReaction(chan: TextChannel, emoji: String, perm: Int) {
    transaction {
        Reaction.new {
            chanId = chan.idLong
            this.emoji = emoji.toByteArray()
            this.perm = perm
        }
    }
}

fun getChannelReactions(chan: TextChannel): List<Reaction> {
    return transaction {
        Reaction.find { Reactions.chanId eq chan.idLong }.toList()
    }
}

fun removeChannelReactions(chan: TextChannel) {
    transaction {
        Reaction.find { Reactions.chanId eq chan.idLong }.forEach { it.delete() }
    }
}