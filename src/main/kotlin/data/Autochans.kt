package data


import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Autochans : IntIdTable("autochans") {
    val chanId = long("chanid")
    val guildId = long("guildid")
    val gameschan = bool("gameschan")
}

class Autochan(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Autochan>(Autochans)

    var chanId by Autochans.chanId
    var guildId by Autochans.guildId
    var gameschan by Autochans.gameschan
}