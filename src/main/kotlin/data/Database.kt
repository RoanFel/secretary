package data

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

class Database(private val host: String, private val port: String, private val user: String, private val password: String, private val database: String) {

    private fun connect() {
        val config = HikariConfig()
        config.jdbcUrl = "jdbc:mysql://$host:$port/$database?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
        config.username = user
        config.password = password

        val hikariDataSource = HikariDataSource(config)

        Database.connect(hikariDataSource)
    }

    fun initialize() {
        connect()
        transaction {
            SchemaUtils.createMissingTablesAndColumns(Autochans, Guilds, UserGuildSupports, Gameschans, BlacklistEntrys,
            Perms, Reactions, EmojiRoles, RoleMenus, UserWarns, Messages)
        }
    }
}
