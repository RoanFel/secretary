package commons

import data.*
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.MessageEmbed.Field
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.jetbrains.exposed.sql.transactions.transaction
import java.awt.Color

/**
 * send a list of the Server Settings to the channel
 */
fun listSettings(event: MessageReceivedEvent) {
    val g = event.guild
    val fields = arrayListOf(
        Field("PREFIX", getPREFIX(g), false),
        Field("SERVER_JOIN_MSG", getSERVERJOINMESSAGE(g), false),
        Field("SERVER_LEAVE_MSG", getSERVERLEAVEMESSAGE(g), false),
        Field("PRIVATE_JOIN_MSG", getPRIVATEJOINMESSAGE(g), false),
        Field("PRIVATE_LEAVE_MSG", getPRIVATELEAVEMESSAGE(g), false),
        Field("CMDLOG_CHANNEL", getCmdlogchan(g), false),
        Field("VOICELOG_CHANNEL", getVoicelogchan(g), false),
        Field("SUPPORT_CHANNEL", getSupportchan(g), false),
        Field("LOG_CHANNEL", getLogchan(g), false),
        Field("DELETE_LOGGING", if(getGuild(g).logDelete) "Ja" else "Nein", false),
        Field("EDIT_LOGGING", if(getGuild(g).logEdit) "Ja" else "Nein", false),
        Field("FEEDBACK_CHANNEL", getFeedbackchan(g), false),
        Field("WARNLOG_CHANNEL", getWarnlogchan(g), false),
        Field("JOIN_MESSAGE_CHANNEL", getJoinmsgchan(g), false),
        Field("LEAVE_MESSAGE_CHANNEL", getLeavemsgchan(g), false),
        Field("PERMROLES_LVL1", getPERMROLES1Names(g).joinToString(", "), false),
        Field("PERMROLES_LVL2", getPERMROLES2Names(g).joinToString(", "), false)
    )
    event.textChannel.sendMessage(
        createEmbed("**GUILD SPECIFIC SETTINGS**", Color.CYAN, "", getAuthor(g),
            g.jda.selfUser.effectiveAvatarUrl, "ServerID: ${g.id}",  fields)).queue()
}

/**
 * get the Prefix of the Server or default if none is set
 */
fun getPREFIX(guild: Guild): String {
    val prefix = getGuild(guild).prefix
    return if (prefix.isBlank()) STATICS.PREFIX else prefix
}

/**
 * set the Server Prefix
 */
fun setPREFIX(entry: String, guild: Guild) {
    transaction {
        getGuild(guild).prefix = entry
    }
}

/**
 * get the Server Joinmessage or OFF if none is set
 */
fun getSERVERJOINMESSAGE(guild: Guild): String {
    val msg = getGuild(guild).joinmsg
    return if (msg.isBlank()) "OFF" else msg
}

/**
 * set the Server Joinmessage
 */
fun setSERVERJOINMESSAGE(entry: String, guild: Guild) {
    transaction {
        getGuild(guild).joinmsg = entry
    }
}

fun getPRIVATEJOINMESSAGE(guild: Guild): String {
    val msg = getGuild(guild).dmjoinmsg
    return if (msg.isBlank()) "OFF" else msg
}

fun setPRIVATEJOINMESSAGE(entry: String, guild: Guild) {
    transaction {
        getGuild(guild).dmjoinmsg = entry
    }
}

/**
 * get the Server Leavemessage or OFF if none is set
 */
fun getSERVERLEAVEMESSAGE(guild: Guild): String {
    val msg = getGuild(guild).leavemsg
    return if (msg.isBlank()) "OFF" else msg
}

/**
 * set the Server Leavemessage
 */
fun setSERVERLEAVEMESSAGE(entry: String, guild: Guild) {
    transaction {
        getGuild(guild).leavemsg = entry
    }
}

fun getPRIVATELEAVEMESSAGE(guild: Guild): String {
    val msg = getGuild(guild).dmleavemsg
    return if (msg.isBlank()) "OFF" else msg
}

fun setPRIVATELEAVEMESSAGE(entry: String, guild: Guild) {
    transaction {
        getGuild(guild).dmleavemsg = entry
    }
}

/**
 * get the Names of the Permroles lvl 1 of the Server or the Default Roles (anyone who can manage Channels) if none is set
 */
fun getPERMROLES1Names(guild: Guild): List<String> {
    return getPERMROLES1(guild).map { it.name }
        .ifEmpty { guild.roles.filter { it.hasPermission(Permission.MANAGE_CHANNEL) }.map { it.name } }
}

/**
 * get the Permroles lvl 1 of the Server (empty list if none is set)
 */
fun getPERMROLES1(guild: Guild): List<Role> {
    return getRoles(guild, getPerms(guild.idLong))
}

fun setPERMROLES1(guild: Guild, roles: List<Role>) {
    roles.forEach { savePerm(guild.idLong, it.idLong) }
}

fun removePERMROLES1(guild: Guild, roles: List<Role>) {
    roles.forEach { removePerm(guild.idLong, it.idLong) }
}

/**
 * get the Names of the Permroles lvl 2 of the Server or the Default Roles (anyone with admin permission) if none is set
 */
fun getPERMROLES2Names(guild: Guild): List<String> {
    return getPERMROLES2(guild).map { it.name }
        .ifEmpty { guild.roles.filter { it.hasPermission(Permission.ADMINISTRATOR) }.map { it.name } }
}

/**
 * get the Permroles lvl 2 of the Server (empty list if none is set)
 */
fun getPERMROLES2(guild: Guild): List<Role> {
    return getRoles(guild, getFullperms(guild.idLong))
}

fun setPERMROLES2(guild: Guild, roles: List<Role>) {
    roles.forEach { saveFullperm(guild.idLong, it.idLong) }
}

fun removePERMROLES2(guild: Guild, roles: List<Role>) {
    roles.forEach { removeFullperm(guild.idLong, it.idLong) }
}


/**
 * get the usernames of the members on the blacklist
 */
fun getBLACKLISTNames(guild: Guild): List<String> {
    return getBLACKLIST(guild).map { it.effectiveName }
}

/**
 * get all members on the Blacklist
 */
fun getBLACKLIST(guild: Guild): List<Member> {
    val blacklist = getBlacklistEntrys(guild.idLong)
    val users: MutableList<Member> = ArrayList()
    blacklist.map {
        val user = guild.getMemberById(it.userId)
        if(user != null) {
            users.add(user)
        }
    }
    return users
}

/**
 * get the cmdlog-channel or <nicht zugewiesen> if there is none with the id
 */
fun getCmdlogchan(guild: Guild): String {
    return guild.getTextChannelById(getCmdchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the cmdlog-channel (-1 of none is set)
 */
fun getCmdchanId(guild: Guild): Long {
    return getGuild(guild).cmdlogchanId
}

/**
 * set the id of the cmdlog-channel
 */
fun setCmdlogchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).cmdlogchanId = entry
    }
}

/**
 * get the voicelog-channel or <nicht zugewiesen> if none with the id
 */
fun getVoicelogchan(guild: Guild): String {
    return guild.getTextChannelById(getVoicelogchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the voicelog-channel (-1 of none is set)
 */
fun getVoicelogchanId(guild: Guild): Long {
    return getGuild(guild).voicelogchanId
}

/**
 * set the id of the voicelog-channel
 */
fun setVoicelogchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).voicelogchanId = entry
    }
}

/**
 * get the support-channel or <nicht zugewiesen> if none with the id
 */
fun getSupportchan(guild: Guild): String {
    return guild.getTextChannelById(getSupportchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the support-channel (-1 of none is set)
 */
fun getSupportchanId(guild: Guild): Long {
    return getGuild(guild).supportchanId
}

/**
 * set the id of the support-channel
 */
fun setSupportchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).supportchanId = entry
    }
}

/**
 * get the log-channel or <nicht zugewiesen> if none with the id
 */
fun getLogchan(guild: Guild): String {
    return guild.getTextChannelById(getLogchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the log-channel (-1 of none is set)
 */
fun getLogchanId(guild: Guild): Long {
    return getGuild(guild).logchanId
}

/**
 * set the id of the log-channel
 */
fun setlogchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).logchanId = entry
    }
}

/**
 * get the feedback-channel or <nicht zugewiesen> if none with the id
 */
fun getFeedbackchan(guild: Guild): String {
    return guild.getTextChannelById(getFeedbackchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the feedback-channel (-1 of none is set)
 */
fun getFeedbackchanId(guild: Guild): Long {
    return getGuild(guild).feedbackchanId
}

/**
 * set the id of the feedback-channel
 */
fun setFeedbackchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).feedbackchanId = entry
    }
}

/**
 * get the warnlog-channel or <nicht zugewiesen> if none with the id
 */
fun getWarnlogchan(guild: Guild): String {
    return guild.getTextChannelById(getWarnlogchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the warnlog-channel (-1 of none is set)
 */
fun getWarnlogchanId(guild: Guild): Long {
    return getGuild(guild).warnlogchanId
}

/**
 * set the id of the warnlog-channel
 */
fun setWarnlogchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).warnlogchanId = entry
    }
}

/**
 * get the joinmsg-channel or <nicht zugewiesen> if none with the id
 */
fun getJoinmsgchan(guild: Guild): String {
    return guild.getTextChannelById(getJoinmsgchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the joinmsg-channel (-1 of none is set)
 */
fun getJoinmsgchanId(guild: Guild): Long {
    return getGuild(guild).joinmsgchanId
}

/**
 * set the id of the joinmsg-channel
 */
fun setJoinmsgchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).joinmsgchanId = entry
    }
}
/**
 * get the leavemsg-channel or <nicht zugewiesen> if none with the id
 */
fun getLeavemsgchan(guild: Guild): String {
    return guild.getTextChannelById(getLeavemsgchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the leavemsg-channel (-1 of none is set)
 */
fun getLeavemsgchanId(guild: Guild): Long {
    return getGuild(guild).leavemsgchanId
}

/**
 * set the id of the leavemsg-channel
 */
fun setLeavemsgchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).leavemsgchanId = entry
    }
}

/**
 * get the privatvoice-channel or <nicht zugewiesen> if none with the id
 */
fun getPrivatvcchan(guild: Guild): String {
    return guild.getVoiceChannelById(getPrivatvcchanId(guild))?.name ?: "<nicht zugewiesen>"
}

/**
 * get the id of the privatvoice-channel (-1 of none is set)
 */
fun getPrivatvcchanId(guild: Guild): Long {
    return getGuild(guild).privatvoiceId
}

/**
 * set the id of the privatvoice-channel
 */
fun setPrivatvcchan(entry: Long, guild: Guild) {
    transaction {
        getGuild(guild).privatvoiceId = entry
    }
}

/**
 * get the Role from a list of Perm (which contains all roleIds of a permlvl)
 */
private fun getRoles(guild: Guild, perms: List<Perm>): List<Role> {
    val roles: MutableList<Role> = ArrayList()
    perms.map {
        val role = guild.getRoleById(it.roleId)
        if(role != null) {
            roles.add(role)
        }
    }
    return roles
}