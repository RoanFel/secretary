package commons

import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.util.*

/**
 * parse a messagereceivedevent in an commandcontainer
 */
fun parse(rw: String, e: MessageReceivedEvent): CommandContainer {
    val beheaded: String = rw.substring(STATICS.PREFIX.length)
    val splitBeheaded = beheaded.split(" ").toTypedArray()
    val split = ArrayList(listOf(*splitBeheaded))
    val invoke = split[0]
    val args = split.subList(1, split.size).toTypedArray()
    return CommandContainer(invoke, args, e)
}

/**
 * class that contains the command, arguments and the event itself from a messagereceivedevent
 */
class CommandContainer internal constructor(val invoke: String, val args: Array<String>, val event: MessageReceivedEvent)