package commons

import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.User
import java.awt.Color
import java.lang.Exception
import java.time.Instant
import java.util.*
import kotlin.concurrent.schedule

/**
 * Create an Embed for success Messages
 */
fun success(g: Guild, description: String, fields: List<MessageEmbed.Field>? = null): MessageEmbed {
    return createEmbed("Erfolg", Color.GREEN, description, getAuthor(g), g.iconUrl, getServerFooter(g), fields)
}

/**
 * Create an Embed for error Messages
 */
fun error(g: Guild, description: String, fields: List<MessageEmbed.Field>? = null): MessageEmbed {

    return createEmbed("Fehler", Color.RED, description, getAuthor(g), g.iconUrl, getServerFooter(g), fields)
}

/**
 * create an Embed with an Title, a Color, a description and optional
 * a AuthorName, AuthorIcon, Footer and a list of EmbedFields
 */
fun createEmbed(title: String?, color: Color, description: String, authorName: String? = null, authorIcon: String? = null
                , footer: String? = null,  fields: List<MessageEmbed.Field?>? = null): MessageEmbed {
    val embed = EmbedBuilder()
    fields?.forEach { if (it != null) embed.addField(it) }
    return embed.setAuthor(authorName, null, authorIcon).setTitle(title).setColor(color)
        .setDescription(description).setFooter(footer).setTimestamp(Instant.now()).build()
}

/**
 * create a SupportTicketEmbed
 */
fun createUserEmbed(title: String, user: User, message: String, color: Color): MessageEmbed {
    return createEmbed(title, color, message, "${user.name}#${user.discriminator}",
        user.effectiveAvatarUrl, "UserID: ${user.id}")
}

/**
 * Return a author-text for embeds for the bot as author in an specific guild
 */
fun getAuthor (guild: Guild): String {
    return "Sekretärin von der Führung vom Server ${guild.name}"
}

/**
 * Return a Footer for the Server-ID
 */
fun getServerFooter (guild: Guild): String {
    return "ServerID: ${guild.id}"
}

fun deleteMessage(message: Message, duration: Long) {
    Timer().schedule(duration) {
        try {
            message.delete().queue()
        } catch (e: Exception) {

        }
    }
}