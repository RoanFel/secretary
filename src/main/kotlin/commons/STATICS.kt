package commons

import net.dv8tion.jda.api.entities.Activity.watching
import java.awt.Color
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * The Properties of the Bot that never changed during Runtime (only at start some Properties are set)
 */
object STATICS {

    const val VERSION = "0.0.1"
    const val PREFIX = "!"

    // the channelnames for the statsvoices
    const val STATSMEMBERS = "Alle Siedler: "
    const val STATSNONBOTS = "Siedler (ohne Bots): "
    const val STATSBOTS = "Bots: "
    const val STATSONLINE = "Online: "
    const val STATSINVOICE = "Siedler am plaudern: "

    // the text for the profile of the bot
    val ACTIVITY = watching(" !help für Hilfe | v.$VERSION")

    // was set from the ReadyListener everytime the Bot starting new
    lateinit var lastRestart: LocalDateTime

    // Settings that are read from the SETTINGS.json file (Token, Owner-Id and SQL Properties)
    var TOKEN = ""
    var BOT_OWNER_ID: Long = 0
    var SQL_HOST = ""
    var SQL_PORT = ""
    var SQL_USER = ""
    var SQL_PASS = ""
    var SQL_DB = ""

}

/**
 * an enum of all command-types, each command has one of these types
 */
enum class CMDTYPE(val cmd: String) {
    ADMINISTRATION("Administration"), CHATUTILS("Chat"), ESSENTIALS("Allgemein"), ETC("Sonstiges"),
    GUILDADMIN("Serververwaltung"), SETTINGS("Einstellungen")
}

/**
 * get the Current Time in Format dd.MM.yyyy hh:mm:ss (z.B. 01.03.2020 12:23:34
 */
fun getCurrentSystemTime(): String {
    val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
    val date = LocalDateTime.now()
    return formatter.format(date)
}

fun getCMDTYPEColors(cmdType: CMDTYPE): Color {
    return when(cmdType) {
        CMDTYPE.ADMINISTRATION -> Color.RED
        CMDTYPE.CHATUTILS -> Color.BLUE
        CMDTYPE.ESSENTIALS -> Color.CYAN
        CMDTYPE.ETC -> Color.GREEN
        CMDTYPE.GUILDADMIN -> Color.ORANGE
        CMDTYPE.SETTINGS -> Color.MAGENTA
    }
}