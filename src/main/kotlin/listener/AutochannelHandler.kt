package listener

import commands.guildAdministration.Autochannel
import commons.createEmbed
import commons.getAuthor
import commons.getPrivatvcchanId
import commons.getServerFooter
import data.Gameschan
import data.Gameschans
import data.getGamesChanByMember
import data.saveGamesChan
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.entities.VoiceChannel
import net.dv8tion.jda.api.events.channel.voice.VoiceChannelDeleteEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import java.awt.Color
import java.util.*
import kotlin.collections.HashMap


class AutochannelHandler : ListenerAdapter() {

    private fun getName(vc: VoiceChannel): String {
        val nameStrings = vc.name.split(' ')
        return if (nameStrings.last().matches("-?\\d+(\\.\\d+)?".toRegex())) {
            nameStrings.subList(0, nameStrings.lastIndex).joinToString(separator = "")
        } else {
            vc.name
        }

    }

    private fun getVcEqualNames(vc: VoiceChannel, autochans: HashMap<VoiceChannel, Pair<Guild, Boolean>>): List<VoiceChannel> {
        val name = getName(vc)
        return autochans.keys.filter { it.name.startsWith(name) }.sortedBy { it.position }

    }

    private fun renameVoices(vcList: List<VoiceChannel>) {
        var i = 1
        vcList.forEach {
            val name = getName(it)
            it.manager.setName("$name $i").queue()
            i++
        }
    }

    private fun createGameVoice(vc: VoiceChannel, g: Guild, m: Member) {
        val activities = m.activities
        if(activities.isEmpty()) {
            val channel = m.user.openPrivateChannel().complete()
            channel.sendMessage(
                createEmbed("Gameschannel", Color.CYAN, "Es ist keine Aktivität im Profil vorhanden." +
                        " Bitte gebe an wie der Channel heißen soll (2-100 Zeichen)", getAuthor(g), g.iconUrl, getServerFooter(g))
                    ).queue()
            userGamesChans[m.user] = UserGameschan(vc, g, m)
            return
        }
        completeGameChan(vc, g, m, activities.first().name)
    }

    class UserGameschan(val vc: VoiceChannel, val g: Guild, val m: Member)



    private fun createVoice(vc: VoiceChannel, g: Guild, m: Member, autochans: HashMap<VoiceChannel, Pair<Guild, Boolean>>) {
        if (vc.members.size > 1) return
        if (autochans.containsKey(vc)) {
            if (autochans[vc]!!.second) {
                createGameVoice(vc, g, m)
                return
            }
            val vcEqualNames = getVcEqualNames(vc, autochans)
            val vcEmpty = vcEqualNames.filter { it.members.size == 0 }

            if (vcEmpty.isNotEmpty()) return

            val nvc = createNewVoice(vc, g, "${getName(vc)} ${vcEqualNames.size + 1}")
            g.modifyVoiceChannelPositions().selectPosition(nvc).moveTo(vcEqualNames.last().position + 1).queue()
            Autochannel.saveVchan(nvc, g, false)
            renameVoices(vcEqualNames)
        }
    }

    private fun deleteVoice(vc: VoiceChannel, autochans: HashMap<VoiceChannel, Pair<Guild, Boolean>>) {
        if (vc.members.size != 0) return
        if (autochans.containsKey(vc)) {
            if(autochans[vc]!!.second) {
                return
            }
            val vcEqualNames = getVcEqualNames(vc, autochans).toMutableList()
            val vcEmpty = vcEqualNames.filter { it.members.size == 0 }.toMutableList()
            vcEmpty.removeAt(0)
            vcEmpty.forEach { key ->
                run {
                    if (key.members.size == 0) {
                        Autochannel.unsetChan(key)
                        key.delete().queue()
                        vcEqualNames.remove(key)
                    }
                }
            }
            if (vcEqualNames.size == 1) {
                val chan = vcEqualNames.first()
                val name = getName(chan)
                chan.manager.setName(name).queue()
            } else {
                renameVoices(vcEqualNames)
            }
        } else {
            transaction {
                val gameschans = Gameschan.find { (Gameschans.chanId eq vc.idLong) and (Gameschans.guildId eq vc.guild.idLong) }
                if(!gameschans.empty()) {
                    vc.delete().queue()
                    gameschans.forEach {it.delete()}
                }
            }
        }
    }

    private fun getVoicePermissions(): EnumSet<Permission> {
        val perms = Permission.getPermissions(Permission.ALL_VOICE_PERMISSIONS)
        perms.addAll(listOf(
            Permission.CREATE_INSTANT_INVITE,
            Permission.MANAGE_CHANNEL,
            Permission.MANAGE_PERMISSIONS,
            Permission.PRIORITY_SPEAKER))
        return perms
    }

    private fun createPrivatVoice(vc: VoiceChannel, g: Guild, m: Member): Boolean {
        val chans = getGamesChanByMember(m)
        if(chans.isNotEmpty()) {
            val chan = g.getVoiceChannelById(chans.first().chanId)
            if(chan != null) {
                g.moveVoiceMember(m, chan).queue()
                return true
            }
        }
        val chanid = getPrivatvcchanId(g)
        if(chanid != vc.idLong) return false
        val action = g.createVoiceChannel(m.effectiveName + "'s Haus")
        if(vc.parent != null) action.setParent(vc.parent)

        action.addPermissionOverride(g.publicRole, null, getVoicePermissions())
        action.addPermissionOverride(m, getVoicePermissions(), null)
        val nvc = action.complete()
        g.moveVoiceMember(m, nvc).queue()
        saveGamesChan(nvc, m)

        return true
    }

    override fun onGuildVoiceJoin(event: GuildVoiceJoinEvent) {
        if(createPrivatVoice(event.channelJoined, event.guild, event.member)) return
        val autochans = Autochannel.load(event.jda)
        createVoice(event.channelJoined, event.guild, event.member, autochans)
    }

    override fun onGuildVoiceMove(event: GuildVoiceMoveEvent) {
        val pvc = (createPrivatVoice(event.channelJoined, event.guild, event.member))
        val autochans = Autochannel.load(event.jda)
        deleteVoice(event.channelLeft, autochans)
        if(pvc) return
        createVoice(event.channelJoined, event.guild, event.member, autochans)
    }

    override fun onGuildVoiceLeave(event: GuildVoiceLeaveEvent) {
        val autochans = Autochannel.load(event.jda)
        deleteVoice(event.channelLeft, autochans)
    }

    override fun onVoiceChannelDelete(event: VoiceChannelDeleteEvent) {
        Autochannel.unsetChan(event.channel)
    }

    companion object {
        val userGamesChans = HashMap<User, UserGameschan>()

        fun completeGameChan(vc: VoiceChannel, g: Guild, m: Member, name: String) {
            val nvc = createNewVoice(vc, g, name)
            g.modifyVoiceChannelPositions().selectPosition(nvc).moveTo(vc.position + 1).queue()
            g.moveVoiceMember(m, nvc).queue()
            saveGamesChan(nvc)
        }

        private fun createNewVoice(vc: VoiceChannel, g: Guild, s: String): VoiceChannel {
            val action = g.createVoiceChannel(s).setBitrate(vc.bitrate)
            vc.permissionOverrides.forEach { p ->
                run {
                    if (p.permissionHolder != null) {
                        action.addPermissionOverride(p.permissionHolder!!, p.allowed, p.denied)
                    }
                }
            }
            val nvc = action.complete() as VoiceChannel
            if (vc.parent != null) nvc.manager.setParent(vc.parent).queue()
            return nvc
        }
    }
}