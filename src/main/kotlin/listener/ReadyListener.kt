package listener

import commands.guildAdministration.Autochannel
import commons.STATICS
import data.GuildDB
import data.messageCacheScheduler
import data.saveGuild
import data.warnScheduler
import net.dv8tion.jda.api.entities.ChannelType
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageHistory
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.time.LocalDateTime
import java.util.function.Consumer


class ReadyListener : ListenerAdapter() {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun onReady(event: ReadyEvent) {
        val sb = StringBuilder()
        event.jda.guilds.forEach(Consumer { guild: Guild -> sb.append("|  - \"${guild.name}\" - {@${guild.owner?.user?.name}#${guild.owner?.user?.discriminator}} - [${guild.id}] ".padEnd(101) + "|\n") })
        logger.info((String.format(
                "\n\n" +
                        "#${"-".repeat(100)}#\n" +
                        "| %s - v.%s (JDA: v.%s)" + "\t".repeat(16) + " |\n" +
                        "#${"-".repeat(100)}#\n" +
                        "| Running on %s guilds: " + "\t".repeat(20) + " |\n" +
                        "%s" +
                        "#${"-".repeat(100)}#\n\n",
                "RoanBot", STATICS.VERSION, "3.3.1_276", event.jda.guilds.size, sb.toString())))
        if (STATICS.BOT_OWNER_ID == 0L) {
            logger.error(
                    "#######################################################\n" +
                            "# PLEASE INSERT YOUR DISCORD USER ID IN SETTINGS.TXT  #\n" +
                            "# AS ENTRY 'BOT_OWNER_ID' TO SPECIFY THAT YOU ARE THE #\n" +
                            "# BOTS OWNER!                                         #\n" +
                            "#######################################################"
            )
        }

        /*
        event.jda.guilds.forEach {
            val file = File(it.name + ".txt")
            val bw = BufferedWriter(FileWriter(file, true))
            file.createNewFile()

            it.channels.forEach {c ->
                if(c is TextChannel) {
                    bw.write(c.name + "\n\n")
                    val history = MessageHistory(c)
                    while (true) {
                        val msgs = history.retrievePast(100).complete()
                        if (msgs.isEmpty()) break
                        msgs.forEach {m ->
                            bw.write("${m.timeCreated} ${m.author}:\n${m.contentDisplay}\n")
                        }
                    }
                    bw.write("\n\n--------------------------------------------\n\n")
            } }

            bw.close()
        }
        */

        transaction {
            val gList = GuildDB.all()
            event.jda.guilds.forEach {
                val entrys = gList.filter { g -> g.guildId == it.idLong }
                if (entrys.count() == 0) {
                    saveGuild(it.idLong)
                }
                if (entrys.count() > 1) {
                    val deleteEntrys = entrys.dropLast(1)


                    deleteEntrys.forEach { entry ->
                        entry.delete()
                    }
                }
            }
        }

        STATICS.lastRestart = LocalDateTime.now()

        Autochannel.load(event.jda)
        warnScheduler()
        messageCacheScheduler()

    }
}