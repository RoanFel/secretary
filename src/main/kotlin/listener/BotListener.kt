package listener

import commands.essentials.Ping.Companion.inputTime
import commands.etc.BotStats.Companion.incrementMessagesProcessed
import commands.guildAdministration.Blacklist
import commons.*
import commons.STATICS.PREFIX
import core.PermCheck
import core.Secretary
import data.Reaction
import data.getChannelReactions
import data.saveMessage
import net.dv8tion.jda.api.entities.ChannelType
import net.dv8tion.jda.api.entities.MessageType
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.jetbrains.exposed.sql.transactions.transaction
import java.awt.Color
import java.time.LocalDateTime


class BotListener : ListenerAdapter() {

    private fun noDeletion(event: MessageReceivedEvent): Boolean {
        val name = event.channel.name
        val guild = event.guild
        return name == getCmdlogchan(guild) ||
                name == getVoicelogchan(guild) ||
                name == getLogchan(guild)
    }

    private fun getReactionChannel(event: MessageReceivedEvent): List<TextChannel> {
        return transaction {
            val reactions = Reaction.all()
            event.guild.textChannels.filter { reactions.any {react -> react.chanId == it.idLong} }
        }
    }

    private fun setReactions(event: MessageReceivedEvent): Boolean {
        var set = false
        val reacts = getChannelReactions(event.textChannel)
        reacts.forEach {
            if((it.perm < 0 || (it.perm==0 && PermCheck.getLvl(event.member) == 0)) || (it.perm > 0 && PermCheck.getLvl(event.member) > it.perm )) {
                event.message.addReaction(it.emoji.toString(Charsets.UTF_8)).queue()
                set = true
            }
        }
        return set
    }

    override fun onMessageReceived(event: MessageReceivedEvent) {
        inputTime = LocalDateTime.now()
        incrementMessagesProcessed()
        val selfMessage = event.author.id == event.jda.selfUser.id
        if (event.channelType == ChannelType.PRIVATE) return
        if (event.message.type == MessageType.CHANNEL_PINNED_ADD) deleteMessage(event.message, 1000)
        if (noDeletion(event)){
            if(!event.author.isBot) deleteMessage(event.message, 1000)
            return
        }
        if(event.message.type == MessageType.DEFAULT) {
            if(!event.message.author.isBot) {
                saveMessage(event.message)
            }
        }
        if (event.channel.name == getSupportchan(event.guild)) {
            if (!selfMessage) {
                answerSupport(event)
            }
            return
        }
        if (event.channel.name == getFeedbackchan(event.guild)) {
            if(PermCheck.check(1, event)) {
                event.message.addReaction("U+1F44D").queue()
                event.message.addReaction("U+1F44E").queue()
                return
            }
        }
        val chans = getReactionChannel(event)
        if (chans.contains(event.channel)) {
            if(setReactions(event)) return
        }
        if (selfMessage) {
            //deleteMessage(event.message)
            return
        }
        if (event.message.contentRaw.startsWith(PREFIX) && !selfMessage) {
            if (!Blacklist.check(event.author, event.guild)) return
            Secretary.handleCommand(parse(event.message.contentRaw, event))
        }
    }

    private fun answerSupport(event: MessageReceivedEvent) {
        val userid = event.message.contentRaw.substringBefore(":").trim()
        val message = event.message.contentRaw.substringAfter(":").trimStart()


        val member = event.guild.members.find { it.id == userid }

        if (member == null) {
            val m = event.message.textChannel.sendMessage(
                error(event.guild, "Es gibt kein Mitglied auf dem Server mit der userid  $userid \n" +
                        "Bitte im Format {userid}: {Nachricht} eingeben")).complete()
            deleteMessage(m, 10000)
        } else {
            member.user.openPrivateChannel().complete().sendMessage(createEmbed("Ticket-Antwort", Color.MAGENTA,
                message, getAuthor(event.guild), event.guild.iconUrl, getServerFooter(event.guild))).queue()
            event.message.textChannel.sendMessage(success(event.guild,
                "Nachricht wurde erfolreich an ${member.asMention} geschickt:\n\n$message")).queue()
        }
    }
}
