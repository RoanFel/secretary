package listener

import commons.createEmbed
import data.*
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed.Field
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.events.message.guild.react.GenericGuildMessageReactionEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import java.awt.Color
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.concurrent.schedule

class RoleMenuConfigurationListener : ListenerAdapter() {

    private fun successMessage(message: String, error: Boolean): String{
        return "\n\n`${if(error) "❌" else "✅"} $message`"
    }

    class Rolemenu (val message: Message, var menu: Int, var timerTask: TimerTask) {
        var channel: TextChannel? = null
        var title: String? = null
        var description: String? = null
        val emojiRoles = HashMap<String, Role>()
        var remove = true
        var multiple = true
        var emojiCache: String? = null
        var roleCache: Role? = null
        var messageMenu: Message? = null
        var editMenu = false
    }

    private fun createRoleMenu(rolemenu: Rolemenu) {
        val chan = rolemenu.channel ?: return
        val title = rolemenu.title ?: return
        val desc = rolemenu.description ?: return
        val emojiRoles = rolemenu.emojiRoles
        if (emojiRoles.isEmpty()) return

        val sb = StringBuilder()
        emojiRoles.forEach { sb.append("${it.key} → ${it.value.asMention}\n")  }
        val fields = listOf(
            Field("Auswahl", sb.toString(), false )
        )

        lateinit var message: Message
        if(!rolemenu.editMenu) {
            message = chan.sendMessage(createEmbed(title, Color.CYAN, desc, fields = fields)).complete()
        } else {
            message = rolemenu.messageMenu ?: return
            message.editMessage(createEmbed(title, Color.CYAN, desc, fields = fields)).complete()
            message.clearReactions().complete()
        }

        emojiRoles.forEach { message.addReaction( it.key ).queue() }

        rolemenu.message.delete().queue()
        rolemenus.remove(rolemenu)

        if(rolemenu.editMenu) {
            deleteRolemenu(message.idLong)
            removeEmojiRoles(message.idLong)
        }

        saveRolemenu(message.idLong, chan.idLong, title, desc, rolemenu.remove, rolemenu.multiple)
        saveEmojiRoles(message.idLong, emojiRoles)
    }

    private fun chooseMenu(rolemenu: Rolemenu) {
        val message = rolemenu.message
        rolemenu.menu = 0
        message.editMessage(createEmbed("**Rollenmenü**", Color.CYAN,
                "Erstelle ein neues Rollenmenü oder bearbeite ein schon vorhandenes.\n\n" +
                        "Rollenmenüs dienen dazu das über Reaktionen automatisch Rollen zugeteilt werden.\n\n\n\n\n",
                fields = listOf(Field("Optionen", emojis["a"] + " - Neues Rollenmenü erstellen\n" +
                        emojis["b"] + " - Rollenmenü bearbeiten", false)))).queue()
    }

    private fun creationMenu(rolemenu: Rolemenu, success: String = "") {
        val message = rolemenu.message
        val chan = rolemenu.channel
        rolemenu.menu = 1

        message.editMessage(createEmbed("**Neues Menü erstellen**", Color.CYAN,
                "Schreibe den Namen oder die ID von dem Kanal in der das Menü erstellt werden soll in diesen Kanal.\n\n" +
                        "Kanal: ${chan?.asMention ?: "<Leer>"}", fields =
                if(chan == null) null else listOf(Field("Optionen",emojis["a"] + " - **Weiter**" +
                success, false)))).queue()
    }

    private fun editMenu(rolemenu: Rolemenu, success: String? = null) {
        val message = rolemenu.message
        val messageMenu = rolemenu.messageMenu
        rolemenu.menu = 2
        message.editMessage(createEmbed("**Menü bearbeiten**", Color.CYAN,
                "Schreibe die ID des Rollenmenüs, das bearbeitet werden soll in diesen Kanal. \n\n" +
                        "MessageID: ${messageMenu?.id ?: "<Leer>"}", fields =
            if(messageMenu == null) null else listOf(Field("Optionen",emojis["a"] + " - **Weiter**" +
                    success, false)))).queue()
    }

    private fun configurationMenu(rolemenu: Rolemenu, success: String = "") {
        val message = rolemenu.message
        val title = rolemenu.title
        val description = rolemenu.description
        val remove = rolemenu.remove
        val multiple = rolemenu.multiple
        val emojiRoles = rolemenu.emojiRoles

        val sb = StringBuilder()
        emojiRoles.forEach { sb.append("${it.key} → ${it.value.asMention}\n")  }
        val s = sb.toString()

        rolemenu.menu = 3
        message.editMessage(createEmbed("**Konfiguration des neuen Menüs**", Color.CYAN,
                "Bite alle Felder ausfüllen und mindestens eine Auswahl hinzufügen\n\n" +
                        "Überschrift:\n" +
                        "${title ?: "<Leer>"}\n" +
                        "Beschreibung:\n" +
                        "${description ?: "<Leer>"}\n" +
                        "Auswahl:\n" +
                        (if (s.isNotBlank()) s else "<Leer>\n") +
                        "Rollenrücknahme bei Entfernung der Reaktion: ${if (remove) emojis["check"] else emojis["x"]}\n" +
                        "Mehrere Rollen gleichzeitig: ${if (multiple) emojis["check"] else emojis["x"]}\n\n", fields =
            listOf(Field("Optionen", emojis["a"] + " - Überschrift ändern\n" +
                        emojis["b"] + " - Beschreibung ändern\n" +
                        emojis["c"] + " - Auswahl hinzufügen\n" +
                        emojis["d"] + " - Auswahl entfernen\n" +
                        emojis["e"] + " - Rollenrücknahme An/Aus\n" +
                        emojis["f"] + " - Mehrere Rollen An/Aus" +
                        (if (title != null && description != null && emojiRoles.isNotEmpty()) "\n" + emojis["g"] +
                                " - **Abschließen**" else null +
                        success), false)))).queue()
    }

    private fun titleMenu(rolemenu: Rolemenu) {
        val message = rolemenu.message
        rolemenu.menu = 4
        message.editMessage(createEmbed("**Überschrift**", Color.CYAN,
            "Schreibe die Überschrift für das Rollenmenü in diesen Kanal.")).queue()
    }

    private fun descriptionMenu(rolemenu: Rolemenu) {
        val message = rolemenu.message
        rolemenu.menu = 5
        message.editMessage(createEmbed("**Beschreibung**", Color.CYAN,
            "Schreibe die Beschreibung für das Rollenmenü in diesen Kanal.")).queue()
    }

    private fun setEmojiRoleMenu(rolemenu: Rolemenu, success: String? = null) {
        val message = rolemenu.message
        val emoji = rolemenu.emojiCache
        val role = rolemenu.roleCache
        val field =  if(emoji != null && role != null) Field("Optionen", emojis["a"] + " - **Hinzufügen**" + success, false) else null

        val fields: List<Field?> = listOf(field)

        rolemenu.menu = 6
        message.editMessage(createEmbed("**Auswahl hinzufügen**", Color.CYAN,
        "Schreibe die ID oder den Namen der Rolle in diesen Kanal und füge das Emoji als Reaktion hinzu:\n\n" +
                "Emoji: ${emoji ?: "<Leer>"}\n" +
                "Rolle: ${role?.asMention ?: "<Leer>"}" + if(fields.isEmpty()) success else "", fields = fields)).queue()
    }

    private fun removeEomjiRoleMenu(rolemenu: Rolemenu) {
        val message = rolemenu.message
        val emojiRoles = rolemenu.emojiRoles
        val sb = StringBuilder()
        emojiRoles.forEach { sb.append("${it.key} → ${it.value.asMention}\n")  }
        val fields = listOf(
            Field("Optionen", sb.toString(), false )
        )

        rolemenu.menu = 7
        message.editMessage(createEmbed("**Auswahl entfernen**", Color.CYAN,
            "Reagiere mit dem Emoji das entfernt werden soll", fields = fields
            )).queue()
    }

    private fun toggleRemoveOrMultipleMenu(rolemenu: Rolemenu, remove: Boolean) {
        if(remove) rolemenu.remove = !rolemenu.remove else rolemenu.multiple = !rolemenu.multiple
        val text = if(remove) "Rollenrücknahme wurde erfolgreich ${if(rolemenu.remove) "hinzugefügt" else "entfernt"}"
        else "Mehrere Rollen gleichzeitig wurde erfolgreich ${if(rolemenu.multiple) "hinzugefügt" else "entfernt"}"
        configurationMenu(rolemenu, successMessage(text, false))
    }

    private fun chooseMenuAction(event: GenericGuildMessageReactionEvent, rolemenu: Rolemenu) {
        val message = rolemenu.message
        when(event.reactionEmote.name) {
            emojis["x"] -> {
                message.delete().queue()
                rolemenus.remove(rolemenu)
                return
            }
            emojis["a"] -> {
                creationMenu(rolemenu)
            }
            emojis["b"] -> {
                editMenu(rolemenu)
            }
        }
    }

    private fun creationMenuAction(event: GenericGuildMessageReactionEvent, rolemenu: Rolemenu) {
        when(event.reactionEmote.name) {
            emojis["x"] -> {
                chooseMenu(rolemenu)
            }
            emojis["a"] -> {
                rolemenu.channel ?: return
                rolemenu.editMenu = false
                configurationMenu(rolemenu)
            }
        }
    }

    private fun creationMenuActionMessage(event: MessageReceivedEvent, rolemenu: Rolemenu) {
        val chanId = event.message.contentRaw
        val chan = if(chanId.toLongOrNull() != null) {
            event.guild.getTextChannelById(chanId)
        } else {
            event.guild.getTextChannelsByName(chanId, true).firstOrNull()
        }
        if(chan == null) {
            creationMenu(rolemenu, successMessage("Es gibt keinen Kanal mit der Id oder den Namen $chanId", true))
        }

        rolemenu.channel = chan
        creationMenu(rolemenu, successMessage("Kanal wurde hnzugefügt", false))
    }

    private fun configurationMenuAction(event: GenericGuildMessageReactionEvent, rolemenu: Rolemenu) {
        when(event.reactionEmote.name) {
            emojis["x"] -> {
                creationMenu(rolemenu)
            }
            emojis["a"] -> {
                titleMenu(rolemenu)
            }
            emojis["b"] -> {
                descriptionMenu(rolemenu)
            }
            emojis["c"] -> {
                if(rolemenu.emojiRoles.size >= 20) {
                    configurationMenu(rolemenu, successMessage("Es wurde die maximale Anzahl von 20 Rollen-Auswahl erreicht", true))
                    return
                }
                setEmojiRoleMenu(rolemenu)
            }
            emojis["d"] -> {
                if(rolemenu.emojiRoles.size == 0) {
                    configurationMenu(rolemenu, successMessage("Es ist noch keine Auswahl vorhanden", true))
                    return
                }
                removeEomjiRoleMenu(rolemenu)
            }
            emojis["e"] -> {
                toggleRemoveOrMultipleMenu(rolemenu, true)
            }
            emojis["f"] -> {
                toggleRemoveOrMultipleMenu(rolemenu, false)
            }
            emojis["g"] -> {
                if (rolemenu.title != null && rolemenu.description != null && rolemenu.emojiRoles.isNotEmpty()) {
                    createRoleMenu(rolemenu)
                }
            }
        }
    }

    private fun editMenuAction(event: GenericGuildMessageReactionEvent, rolemenu: Rolemenu) {
        when(event.reactionEmote.name) {
            emojis["x"] -> {
                chooseMenu(rolemenu)
            }
            emojis["a"] -> {
                rolemenu.channel ?: return
                rolemenu.editMenu = true
                configurationMenu(rolemenu)
            }
        }
    }

    private fun editMenuActionMessage(event: MessageReceivedEvent, rolemenu: Rolemenu) {

        val messageId = event.message.contentRaw
        val rolemenuDb = getRolemenu(messageId.toLongOrNull())

        if (rolemenuDb == null) {
            editMenu(rolemenu, successMessage("Es existiert kein Rollenmenü mit der ID $messageId", true))
            return
        }

        val channel = event.guild.getTextChannelById(rolemenuDb.chanId)
        val message = channel?.retrieveMessageById(rolemenuDb.messageId)?.complete()
        if(message == null) {
            editMenu(rolemenu, successMessage("Es existiert kein Rollenmenü mit der ID $messageId", true))
            deleteRolemenu(messageId.toLong())
            return
        }

        rolemenu.messageMenu = message
        rolemenu.channel = channel
        rolemenu.remove = rolemenuDb.remove
        rolemenu.multiple = rolemenuDb.multiple
        rolemenu.title = rolemenuDb.title
        rolemenu.description = rolemenuDb.description
        val emojiRoles = getEmojiRoles(message.idLong)
        emojiRoles.map { val role = event.guild.getRoleById(it.roleId)
            if(role != null) {
                rolemenu.emojiRoles[it.emoji.toString(Charsets.UTF_8)] = role
            }
        }

        editMenu(rolemenu, successMessage("Rollenmenü ausgewählt", false))
    }

    private fun titleMenuAction(event: GenericGuildMessageReactionEvent, rolemenu: Rolemenu) {
        when(event.reactionEmote.name) {
            emojis["x"] -> {
                configurationMenu(rolemenu)
            }
        }
    }

    private fun titleMenuActionMessage(event: MessageReceivedEvent, rolemenu: Rolemenu, title: Boolean) {
        val message = event.message.contentRaw
        if(title) rolemenu.title = message else rolemenu.description = message

        configurationMenu(rolemenu, successMessage("Die ${if (title) "Überschrift" else "Beschreiung"} wurde geändert.", false))
    }

    private fun setEmojiRoleMenuAction(event: GenericGuildMessageReactionEvent, rolemenu: Rolemenu) {
        val emojiCache = rolemenu.emojiCache
        val roleCache = rolemenu.roleCache
        val emoji = event.reactionEmote.name
        when(emoji) {
            emojis["x"] -> {
                rolemenu.roleCache = null
                rolemenu.emojiCache = null
                configurationMenu(rolemenu)
                return
            }
            emojis["a"] -> {
                if(roleCache != null && emojiCache != null) {
                    rolemenu.emojiRoles[emojiCache] = roleCache
                    rolemenu.roleCache = null
                    rolemenu.emojiCache = null
                    configurationMenu(rolemenu, successMessage("Auswahl hinzugefügt", false))
                    return
                } else {
                    if(rolemenu.emojiRoles.containsKey(emoji)) {
                        setEmojiRoleMenu(rolemenu, successMessage("Das Emoji $emoji ist bereits zugeteilt", true))
                    }
                    rolemenu.emojiCache = emoji
                }
            }
            else -> {
                if(rolemenu.emojiRoles.containsKey(emoji)) {
                    setEmojiRoleMenu(rolemenu, successMessage("Das Emoji $emoji ist bereits zugeteilt", true))
                }
                rolemenu.emojiCache = emoji
            }
        }
        if(!emojis.values.contains(emoji)) {
            rolemenu.message.clearReactions(emoji).queue()
        }
        setEmojiRoleMenu(rolemenu, successMessage("Das Emoji wurde geändert", false))
    }

    private fun setEmojiRoleMenuActionMessage(event: MessageReceivedEvent, rolemenu: Rolemenu) {
        val roleId = event.message.contentRaw
        val role = if(roleId.toLongOrNull() != null) {
            event.guild.getRoleById(roleId)
        } else {
            event.guild.getRolesByName(roleId, true).firstOrNull()
        }

        if(role == null) {
            setEmojiRoleMenu(rolemenu, successMessage("Es gibt keine Rolle mit dem Namen oder der Id $roleId", true))
            return
        }

        rolemenu.roleCache = role
        setEmojiRoleMenu(rolemenu, successMessage("Die Rolle wurde geändert.", false))
    }

    private fun removeEmojiRoleMenuAction(event: GenericGuildMessageReactionEvent, rolemenu: Rolemenu) {
        when(val emoji = event.reactionEmote.name) {
            emojis["x"] -> {
                configurationMenu(rolemenu)
            }
            else -> {
                val role = rolemenu.emojiRoles.remove(emoji)
                if (role == null) {
                    configurationMenu(rolemenu, successMessage("Es gibt keine Auswah mit dem emoji $emoji", true))
                } else {
                    configurationMenu(rolemenu, successMessage("Auswahl $emoji -> ${role.name} wurde entfernt", false))
                }
                if(!emojis.values.contains(emoji)) {
                    rolemenu.message.clearReactions(emoji).queue()
                }
            }
        }
    }


    override fun onGenericGuildMessageReaction(event: GenericGuildMessageReactionEvent) {
        if(event.user == event.jda.selfUser) return
        val rolemenu = rolemenus.find { it.message.id == event.messageId } ?: return

        rolemenu.timerTask.cancel()

        when(rolemenu.menu) {
            0 -> chooseMenuAction(event, rolemenu)
            1 -> creationMenuAction(event, rolemenu)
            2 -> editMenuAction(event, rolemenu)
            3 -> configurationMenuAction(event, rolemenu)
            4, 5 -> titleMenuAction(event, rolemenu)
            6 -> setEmojiRoleMenuAction(event, rolemenu)
            7 -> removeEmojiRoleMenuAction(event, rolemenu)
        }

        if(rolemenus.contains(rolemenu)) rolemenu.timerTask = deleteMessage(rolemenu.message)
    }

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if(event.author == event.jda.selfUser) return
        val chan = event.channel
        val message = chan.history.retrievePast(2).complete().last()
        val rolemenu = rolemenus.find { it.message == message } ?: return

        rolemenu.timerTask.cancel()

        when(rolemenu.menu) {
            1 -> creationMenuActionMessage(event, rolemenu)
            2 -> editMenuActionMessage(event, rolemenu)
            4 -> titleMenuActionMessage(event, rolemenu, true)
            5 -> titleMenuActionMessage(event, rolemenu, false)
            6 -> setEmojiRoleMenuActionMessage(event, rolemenu)
        }

        event.message.delete().queue()
        if(rolemenus.contains(rolemenu)) rolemenu.timerTask = deleteMessage(rolemenu.message)

    }

    companion object {
        val rolemenus = ArrayList<Rolemenu>()
        val emojis = mapOf(Pair("x", "❌"),Pair("a", "\uD83C\uDDE6"), Pair("b", "\uD83C\uDDE7"), Pair("c", "\uD83C\uDDE8"),
                Pair("d", "\uD83C\uDDE9"), Pair("e", "\uD83C\uDDEA"), Pair("f", "\uD83C\uDDEB"), Pair("g", "\uD83C\uDDEC") ,
                Pair("check", "✅")  )

        fun deleteMessage(message: Message): TimerTask {
            return Timer().schedule(300000) {
                rolemenus.removeAll { it.message == message }
                message.delete().queue()
            }
        }

    }



}