package listener

import commons.*
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import java.awt.Color


class GuiltMemberListener : ListenerAdapter() {

    override fun onGuildMemberJoin(event: GuildMemberJoinEvent) {

        if (event.user.isBot) return

        val guild = event.guild
        val pnJoinMsg = getPRIVATEJOINMESSAGE(guild)
        if (pnJoinMsg.toLowerCase() != "off") {
            event.user.openPrivateChannel().complete().sendMessage(
                createEmbed(null, Color.GREEN, pnJoinMsg
                    .replace("[USER]", event.user.asMention, true)
                    .replace("[SERVER]", guild.name, true)
                    .replace("[BOTNAME]", guild.selfMember.effectiveName, true),
                    getAuthor(guild), guild.iconUrl, getServerFooter(guild))).queue()
        }

        val joinmsg = getSERVERJOINMESSAGE(guild)
        if (joinmsg.toLowerCase() != "off") {
            val chan = guild.getTextChannelById(getJoinmsgchanId(guild)) ?: return

            chan.sendMessage(
                createEmbed(null, Color.GREEN, joinmsg
                    .replace("[USER]", event.user.asMention,true)
                    .replace("[SERVER]", guild.name,true)
                    .replace("[BOTNAME]", guild.selfMember.effectiveName, true),
                    getAuthor(guild), guild.iconUrl, getServerFooter(guild))).queue()
        }
    }

    override fun onGuildMemberRemove(event: GuildMemberRemoveEvent) {
        val user = event.user
        if (user.isBot) return

        val guild = event.guild
        val pnLeaveMsg = getPRIVATELEAVEMESSAGE(guild)
        if (pnLeaveMsg.toLowerCase() != "off") {
            user.openPrivateChannel().complete().sendMessage(
                createEmbed(null, Color.RED, pnLeaveMsg
                    .replace("[USER]", event.user.asMention,true)
                    .replace("[SERVER]", guild.name,true)
                    .replace("[BOTNAME]", guild.selfMember.effectiveName, true),
                    getAuthor(guild), guild.iconUrl, getServerFooter(guild))).queue()
        }

        val leaveMsg = getSERVERLEAVEMESSAGE(guild)
        if (leaveMsg.toLowerCase() != "off") {
            val chan = guild.getTextChannelById(getLeavemsgchanId(guild)) ?: return

            chan.sendMessage(
                createEmbed(null, Color.RED, leaveMsg
                    .replace("[USER]", event.user.asMention,true)
                    .replace("[SERVER]", guild.name,true)
                    .replace("[BOTNAME]", guild.selfMember.effectiveName, true),
                    getAuthor(guild), guild.iconUrl, getServerFooter(guild))).queue()
        }
    }
}