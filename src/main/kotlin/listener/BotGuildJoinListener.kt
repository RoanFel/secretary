package listener

import commons.createEmbed
import data.GuildDB
import data.Guilds
import data.saveGuild
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.jetbrains.exposed.sql.transactions.transaction
import java.awt.Color

class BotGuildJoinListener : ListenerAdapter() {

    override fun onGuildJoin(event: GuildJoinEvent) {
        val guild: Guild = event.guild
        event.guild.retrieveOwner().complete().user.openPrivateChannel().complete().sendMessage(
            createEmbed("Melde mich zum Dienst!", Color.CYAN,
                    "Vielen Dank für die Einladug auf den Server \"${event.guild.name}\"!\n" +
                            "\n" +
                            "Um meine Dienste vollumfänglich nutzen zu können, richte bitte folgende Channel ein:\n" +
                            "\n" +
                            "CMDLOG: Loggt alle Befehle, die auf dem Server an den Bot gegeben werden.\n" +
                            "\n" +
                            "Voicelog: Loggt, wenn jemand auf dem Server einen Voice betritt/verlässt/switcht.\n" +
                            "\n" +
                            "Support: An diesen werden Tickets, die User per DM an den Bot für den Server schicken, geschickt.\n" +
                            "\n" +
                            "Log: Hier werden die gelöschten oder editierten Nachrichten geloggt.\n" +
                            "\n" +
                            "Feedback: Hier wird jede Nachricht eines normalen Mitglieds einen Daumen hoch und runter Emoji hinzugefügt, für Abstimmungen.\n" +
                            "\n" +
                            "Warnlog: Hier werden alle Verwarnungen geloggt. Solange ein Channel nicht zugewiesen wurde, wird die entsprechende Funktion nicht ausgeführt.\n" +
                            "\n" +
                            "Weitere Infos erhalten Sie auf dem Server mit !help im Botchannel.\n" +
                            "\n" +
                            "Eine Übersicht aller Infos erhalte mit !help.\n" +
                            "\n" +
                            "Ich freue mich zu Diensten sein zu können!\n" +
                            "\n" +
                            "Ihre Sektretärin.",
                event.jda.selfUser.name, event.jda.selfUser.effectiveAvatarUrl)).queue()
        saveGuild(guild.idLong)
    }

    override fun onGuildLeave(event: GuildLeaveEvent) {
        transaction {
            GuildDB.find { Guilds.guildId eq event.guild.idLong }.forEach { it.delete() }
        }
    }
}