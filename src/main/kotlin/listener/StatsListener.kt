package listener

import commons.STATICS
import net.dv8tion.jda.api.OnlineStatus
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.guild.member.GenericGuildMemberEvent
import net.dv8tion.jda.api.events.guild.voice.GenericGuildVoiceEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateOnlineStatusEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class StatsListener : ListenerAdapter() {

    override fun onUserUpdateOnlineStatus(event: UserUpdateOnlineStatusEvent) {
        val online = event.guild.members.filter { it.onlineStatus != OnlineStatus.OFFLINE }.count()

        setStatsVoiceChannelNames(event.guild, STATICS.STATSONLINE, online)
    }

    override fun onGenericGuildMember(event: GenericGuildMemberEvent) {
        val members = event.guild.members
        val membersCounts = members.count()
        val bots = members.filter { it.user.isBot }.count()
        val nonBots = membersCounts - bots

        setStatsVoiceChannelNames(event.guild, STATICS.STATSMEMBERS, membersCounts)
        setStatsVoiceChannelNames(event.guild, STATICS.STATSNONBOTS, nonBots)
        setStatsVoiceChannelNames(event.guild, STATICS.STATSBOTS, bots)
    }

    override fun onGenericGuildVoice(event: GenericGuildVoiceEvent) {
        var memberinvoice = 0
        event.guild.voiceChannels.forEach { memberinvoice += it.members.size }

        setStatsVoiceChannelNames(event.guild, STATICS.STATSINVOICE, memberinvoice)
    }

    private fun setStatsVoiceChannelNames(guild: Guild, name: String, count: Int) {
        val vc = guild.voiceChannels.find { it.name.startsWith(name) }
        vc?.manager?.setName(name + count)?.queue()
    }
}