package listener

import data.getEmojiRoles
import data.getRolemenu
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class RoleMenuListener : ListenerAdapter() {

    override fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) {
        val rolemenu = getRolemenu(event.messageIdLong)
        val emojiRoles = ArrayList<Pair<String, Role>>()
        getEmojiRoles(event.messageIdLong).map { val role = event.guild.getRoleById(it.roleId)
            if(role != null) {
                emojiRoles.add(Pair(it.emoji.toString(Charsets.UTF_8), role))
            } }

        val roles = emojiRoles.filter { it.first == event.reactionEmote.name }.map { it.second }

        if(rolemenu == null) {
            roles.forEach {
                event.guild.addRoleToMember(event.member, it).queue()
            }
            return
        }

        if(roles.isEmpty()) {
            event.guild.getTextChannelById(rolemenu.chanId)?.retrieveMessageById(rolemenu.messageId)?.complete()?.
            clearReactions(event.reactionEmote.name)?.queue()
            return
        }

        val role = roles.first()

        if(!rolemenu.multiple) {
            emojiRoles.forEach {
                event.guild.removeRoleFromMember(event.member, it.second).queue()
            }
        }

        event.guild.addRoleToMember(event.member, role).queue()
    }

    override fun onGuildMessageReactionRemove(event: GuildMessageReactionRemoveEvent) {
        val member = event.member ?: return
        val rolemenu = getRolemenu(event.messageIdLong) ?: return
        val emojiRoles = HashMap<String, Role>()
        getEmojiRoles(event.messageIdLong).map { val role = event.guild.getRoleById(it.roleId)
            if(role != null) {
                emojiRoles[it.emoji.toString(Charsets.UTF_8)] = role
            } }
        val role = emojiRoles[event.reactionEmote.name] ?: return

        if(rolemenu.remove) event.guild.removeRoleFromMember(member, role).queue()
    }
}