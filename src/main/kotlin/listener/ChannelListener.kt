package listener

import data.getGuild
import net.dv8tion.jda.api.events.channel.text.TextChannelDeleteEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.jetbrains.exposed.sql.transactions.transaction

class ChannelListener : ListenerAdapter() {

    override fun onTextChannelDelete(event: TextChannelDeleteEvent) {
        val guild = getGuild(event.guild)
        transaction {
            when(event.channel.idLong) {
                guild.cmdlogchanId -> guild.cmdlogchanId = -1
                guild.voicelogchanId -> guild.voicelogchanId = -1
                guild.supportchanId -> guild.supportchanId = -1
                guild.logchanId -> {
                    guild.logchanId = -1
                    guild.logDelete = false
                    guild.logEdit = false
                }
                guild.feedbackchanId -> guild.feedbackchanId = -1
                guild.warnlogchanId -> guild.warnlogchanId = -1
                guild.joinmsgchanId -> guild.joinmsgchanId = -1
                guild.leavemsgchanId -> guild.leavemsgchanId = -1
            }
        }

    }
}