package listener

import commons.*
import data.GuildDB
import data.UserGuildSupport
import data.UserGuildSupports
import data.saveUserGuild
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.jetbrains.exposed.sql.transactions.transaction
import java.awt.Color

class PrivateMessageListener : ListenerAdapter() {

    private class UserMessage(val user: User, val message: Message, val list: List<Guild>)
    private val userMessages = ArrayList<UserMessage>()

    private fun sendMessageToSupport(event: PrivateMessageReceivedEvent, guild: Guild) {

        val message = userMessages.find { it.user == event.author }?.message ?: event.message
        val user = message.author
        val chan = guild.getTextChannelById(getSupportchanId(guild))
        if (chan == null) {
                event.channel.sendMessage(error(guild, "Der Server hat keinen Supportchannel.\n" +
                        "Bitte wende dich dafür direkt an die Teamleitung des Servers.")).queue()
        }
        val embed = createUserEmbed("Ticket", user, message.contentRaw, Color.GREEN)
        chan!!.sendMessage(embed).queue()

        event.channel.sendMessage(success(guild, "Die Nachricht wurde an den Server \"${guild.name}\" gesendet.")
                ).queue()
    }

    private fun getMemberGuilds(event: PrivateMessageReceivedEvent): List<Guild> {
        return transaction {
            val guilds = GuildDB.all()
            return@transaction event.author.mutualGuilds.filter { g -> guilds.any { g.idLong == it.guildId } }
        }
    }

    private fun askServerMessage(event: PrivateMessageReceivedEvent) {
        val memberGuilds = getMemberGuilds(event)

        val sb = StringBuilder()
        val list = ArrayList<String>()

        sb.appendln("An welchen Server soll die Nachricht geschickt werden (\"!close\" zum abbrechen):")
        val i = 1
        memberGuilds.forEach {
            sb.appendln("$i. ${it.name}")
            if (sb.length > 1900) {
                list.add(sb.toString())
                sb.clear()
            }
        }
        list.add(sb.toString())
        val j = 1
        list.forEach {
            event.channel.sendMessage(createEmbed("Server Liste Seite $j", Color.CYAN, it)).queue()
        }
        userMessages.add(UserMessage(event.author, event.message, memberGuilds))
    }

    private fun checkServerNumber(event: PrivateMessageReceivedEvent): Guild? {
        val num = event.message.contentRaw.toIntOrNull()

        val guilds = userMessages.find { it.user == event.author }?.list ?: emptyList()

        if (num == null || num < 1 || num > guilds.size) {
            event.channel.sendMessage(
                createEmbed("Fehler", Color.RED, "Bitte nur die Zahl (1 - ${guilds.size})" +
                        " des gewünschten Servers oder !close für einen Abbruch eingeben!")).queue()
            return null
        }

        return guilds[num - 1]
    }

    override fun onPrivateMessageReceived(event: PrivateMessageReceivedEvent) {

        if (event.author.id == event.jda.selfUser.id) return

        val userGameschan = AutochannelHandler.userGamesChans[event.author]

        if (userGameschan != null) {
            if(event.message.contentRaw.length > 100 || event.message.contentRaw.length < 2) {
                event.channel.sendMessage(error(userGameschan.g, "Der Name darf nur zwischen 2-100 Zeichen lang sein!"))
                return
            }
            AutochannelHandler.completeGameChan(userGameschan.vc, userGameschan.g, userGameschan.m, event.message.contentRaw)
            AutochannelHandler.userGamesChans.remove(event.author)
        }

        if (event.message.contentRaw.trim().toLowerCase() == "!close") {
            transaction {
                UserGuildSupport.find { UserGuildSupports.userId eq event.author.idLong }.firstOrNull()?.delete()
            }
            userMessages.removeAll { it.user == event.author }
            event.channel.sendMessage(
                createEmbed("Erfolg", Color.GREEN, "Das Ticketsystem wurde zurückgesetzt.")).queue()
            return
        }

        val returnFunction = transaction {
            val userguild = UserGuildSupport.find { UserGuildSupports.userId eq event.author.idLong }

            if (!userguild.empty()) {
                val guildId = userguild.first().guildId
                when (val guild = event.jda.getGuildById(guildId)) {
                    null -> {
                        userguild.first().delete()
                    }
                    else -> {
                        sendMessageToSupport(event, guild)
                        return@transaction true
                    }
                }

            }
            return@transaction false

        }

        if (returnFunction) return

        if (userMessages.find { it.user == event.author } == null) {
            askServerMessage(event)
            return
        }

        val guild = checkServerNumber(event) ?: return

        sendMessageToSupport(event, guild)

        saveUserGuild(event.author.idLong, guild.idLong)

        event.channel.sendMessage(
            createEmbed("Server gespeichert", Color.CYAN, "Der Server \"${guild.name}\" " +
                    "wurde für zukünftige Nachrichten gespeichert.\n" +
                    "Wenn nicht mehr an diesen Server geschickt werden soll bitte \"!close\" schreiben")).queue()


        userMessages.removeAll { it.user == event.author }
    }

}

