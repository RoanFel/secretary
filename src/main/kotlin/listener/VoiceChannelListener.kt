package listener

import commons.createUserEmbed
import commons.getVoicelogchanId
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import java.awt.Color


class VoiceChannelListener : ListenerAdapter() {

    override fun onGuildVoiceJoin(event: GuildVoiceJoinEvent) {
        val voicechannel = event.guild.getTextChannelById(getVoicelogchanId(event.guild)) ?: return
        val user = event.member.user
        val message = "${user.asMention} betritt den voice ${event.channelJoined.name}:loud_sound:"
        voicechannel.sendMessage(createUserEmbed("VoiceLog", user, message, Color.GREEN)).queue()
    }

    override fun onGuildVoiceMove(event: GuildVoiceMoveEvent) {
        val voicechannel = event.guild.getTextChannelById(getVoicelogchanId(event.guild)) ?: return
        val user = event.member.user
        val message = "${user.asMention} wechselt von voice ${event.channelLeft.name}:loud_sound:->${event.channelJoined.name}:loud_sound:"
        voicechannel.sendMessage(createUserEmbed("VoiceLog", user, message, Color.YELLOW)).queue()
    }

    override fun onGuildVoiceLeave(event: GuildVoiceLeaveEvent) {
        val voicechannel = event.guild.getTextChannelById(getVoicelogchanId(event.guild)) ?: return
        val user = event.member.user
        val message = "${user.asMention} verlässt den voice ${event.channelLeft.name}:loud_sound:"
        voicechannel.sendMessage(createUserEmbed("VoiceLog", user, message, Color.RED)).queue()
    }
}