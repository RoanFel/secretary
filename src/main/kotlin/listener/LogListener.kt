package listener

import commons.createEmbed
import data.*
import net.dv8tion.jda.api.audit.ActionType
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.guild.GuildMessageDeleteEvent
import net.dv8tion.jda.api.events.message.guild.GuildMessageUpdateEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.jetbrains.exposed.sql.transactions.transaction
import java.awt.Color
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit

class LogListener : ListenerAdapter() {

    private fun getChan(g: Guild, guild: GuildDB): TextChannel? {
        val chan = g.getTextChannelById(guild.logchanId)
        if (chan == null) {
            transaction {
                guild.logchanId = -1
                guild.logDelete = false
                guild.logEdit = false
            }
        }
        return chan
    }

    override fun onGuildMessageDelete(event: GuildMessageDeleteEvent) {
        val guild = getGuild(event.guild)
        if(!guild.logDelete) {
            deleteMessage(event.messageIdLong)
            return
        }

        val chan = getChan(event.guild, guild)
        if(chan == null){
            deleteMessage(event.messageIdLong)
            return
        }

        val message = getMessage(event.messageIdLong) ?: return
        val author = event.jda.getUserById(message.authorId)

        val entry = event.guild.retrieveAuditLogs().complete().find {
            (it.type == ActionType.MESSAGE_DELETE || it.type == ActionType.MESSAGE_BULK_DELETE) &&
                    (it.targetIdLong == message.authorId || it.targetIdLong == message.chanId) &&
                    ChronoUnit.MINUTES.between(it.timeCreated, OffsetDateTime.now()) < 1 }

        chan.sendMessage(createEmbed("**Nachricht gelöscht**", Color.MAGENTA, message.message,
            author?.name, author?.effectiveAvatarUrl,
            "MessageID: ${event.messageId}", listOf(
                MessageEmbed.Field("**Channel:**",  "${event.channel.asMention} `[#${event.channel.name}]`", false),
                MessageEmbed.Field("**Autor:**", author?.asMention ?: "Nicht gefunden, siehe ID", false),
                MessageEmbed.Field("**AutorID:**", message.authorId.toString(), false),
                MessageEmbed.Field("**Gelöscht durch**", if(entry == null) "Autor" else entry.user?.asMention, false)
            ))).queue()

        deleteMessage(event.messageIdLong)
    }

    override fun onGuildMessageUpdate(event: GuildMessageUpdateEvent) {
        val guild = getGuild(event.guild)
        if(!guild.logEdit) {
            updateMessage(event.message)
            return
        }

        val chan = getChan(event.guild, guild)
        if(chan == null) {
            updateMessage(event.message)
            return
        }

        val message = getMessage(event.messageIdLong) ?: return

        val entry = event.guild.retrieveAuditLogs().complete().find { it.type == ActionType.MESSAGE_UPDATE &&
                it.targetIdLong == message.authorId &&
                ChronoUnit.MINUTES.between(it.timeCreated, OffsetDateTime.now()) < 1 }

        var message1 = event.message.contentDisplay
        var message2 = ""
        if(message1.length > 1000) {
            val temp = message1.substring(1000)
            message2 = temp.substringAfter(" ")
            message1 = message1.substring(0, 1000) + temp.substringBefore(" ")
        }

        chan.sendMessage(createEmbed("**Nachricht bearbeitet**", Color.MAGENTA, "**Vorher:**\n" + message.message,
            event.author.name, event.author.effectiveAvatarUrl,
            "MessageID: ${event.messageId}", listOf(
                MessageEmbed.Field("**Nachher${if (message2.isBlank()) "" else "(1)"}:**", message1, false),
                if(message2.isBlank()) null else MessageEmbed.Field("**Nachher(2):**", message2, false),
                MessageEmbed.Field("**Channel:**",  "${event.channel.asMention} `[#${event.channel.name}]`", false),
                MessageEmbed.Field("**Autor:**", event.author.asMention , false),
                MessageEmbed.Field("**AutorID:**", event.author.id, false),
                MessageEmbed.Field("**Bearbeitet durch**", if(entry == null) "Autor" else entry.user?.asMention, false)
            ))).queue()

        updateMessage(event.message)
    }
}